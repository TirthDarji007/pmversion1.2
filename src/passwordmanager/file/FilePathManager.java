package passwordmanager.file;

import java.io.File;

/**
 * Created by Tirth on 7/5/2017.
 */
public class FilePathManager {

    private final String WORK_PATH;
    private final String ROOT_FOLDER = "Password Manager";
    private final String DATA_FOLDER = "Data";
    private final String LOG_FOLDER = "log";
    
    public FilePathManager(){
        WORK_PATH = System.getProperty("user.home");
    }
    
    public String getUserHomePath(){
        return WORK_PATH + File.separatorChar;
    }
    
    public String getWorkPath() {
        return WORK_PATH + File.separatorChar + ROOT_FOLDER + File.separatorChar;
    }

    public String getDataPath(){
        return getWorkPath() + DATA_FOLDER + File.separatorChar ;
    }
    
    public String getLogPath(){
        return getWorkPath() + LOG_FOLDER + File.separatorChar;
    }
}
