/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.file;

import java.io.BufferedReader;
import java.io.File;
import java.util.Date;
import javafx.collections.ObservableList;
import passwordmanager.PasswordManager;
import passwordmanager.dataclasses.PasswordManagerData;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import passwordmanager.dataclasses.BackupObject;
import passwordmanager.dataclasses.Login;
import passwordmanager.dataclasses.User;

/**
 *
 * @author tirth
 */
public class PasswordManagerFiles {

    //SAVE THE APP
    PasswordManager app;
    FilePathManager path = new FilePathManager();
    DateFormat dateFormatter = new SimpleDateFormat("E, MMM dd yyyy HH:mm:ss");

    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_LOGINS = "logins";
    static final String JSON_LOGIN = "login";
    static final String JSON_FILE_NAME = "PasswordManagerSettings";
    static final String JSON_FILE_EXTENSION = ".exe";
    static final String JSON_LOGIN_NAME = "name";
    static final String JSON_LOGIN_USERID = "userID";
    static final String JSON_LOGIN_PASSWORD = "password";
    static final String JSON_LOGIN_WEBSITE = "website";
    static final String JSON_LOGIN_NOTE = "note";

    static final String JSON_USER = "user";
    static final String JSON_USER_NAME = "user_name";
    static final String JSON_USER_USERID = "user_userID";
    static final String JSON_USER_PASSWORD = "user_password";
    static final String JSON_USER_EMAIL = "user_email";
    static final String JSON_USER_RECOVERY = "user_recovery";
    
    static final String JSON_BACKUP = "backup";
    static final String JSON_BACKUP_LOCATION = "backup_location";
    static final String JSON_BACKUP_DAY = "backup_days";
    static final String JSON_BACKUP_DATE = "backup_date";
    static final String JSON_BACKUP_SCHEDULE = "backup_schedule";
    
    static final String CSV_SEPERATOR = ",";

    public PasswordManagerFiles(PasswordManager app) {
        this.app = app;
    }

    /**
     * LOAD DATA FROM DEFAUL FILE LOCATION
     *
     * @param data
     * @throws IOException
     */
    public void loadData(PasswordManagerData data) throws Exception {
        //MAKE SURE DATA FILE EXISTS. IF NOT THROW ERROR
        File file = new File(path.getDataPath() + JSON_FILE_NAME + JSON_FILE_EXTENSION);
        if (!file.exists()) {
            throw new IOException("Data file not found.");
        }

        loadData(data, path.getDataPath() + JSON_FILE_NAME + JSON_FILE_EXTENSION);
    }

    /**
     * LOAD DATA FROM GIVEN FILE LOCATION
     *
     * @param data
     * @throws IOException
     */
    public void loadData(PasswordManagerData data, String location) throws Exception {

        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(location);

        JsonArray jsonLoginArray = json.getJsonArray(JSON_LOGINS);
        for (int i = 0; i < jsonLoginArray.size(); i++) {
            JsonObject jsonLogin = jsonLoginArray.getJsonObject(i);
            String name = jsonLogin.getString(JSON_LOGIN_NAME);
            String userID = jsonLogin.getString(JSON_LOGIN_USERID);
            String password = jsonLogin.getString(JSON_LOGIN_PASSWORD);
            String website = jsonLogin.getString(JSON_LOGIN_WEBSITE);
            String note = jsonLogin.getString(JSON_LOGIN_NOTE);
            data.addLogin(name, userID, password, website, note);
        }

        //LOAD THE USER 
        JsonObject userjson = json.getJsonObject(JSON_USER);
        String userName = userjson.getString(JSON_USER_NAME);
        String userUserID = userjson.getString(JSON_USER_USERID);
        String userPassword = userjson.getString(JSON_USER_PASSWORD);
        String userEmail = userjson.getString(JSON_USER_EMAIL);
        String userRecovery = userjson.getString(JSON_USER_RECOVERY);
        User loadedUser = new User(userUserID, userPassword, userRecovery);
        loadedUser.setName(userName);
        loadedUser.setEmail(userEmail);
        data.setUser(loadedUser);
        
        //LOAD BACKUP DATA
        JsonObject backupJson = json.getJsonObject(JSON_BACKUP);
        String blocation = backupJson.getString(JSON_BACKUP_LOCATION);
        int bday = backupJson.getInt(JSON_BACKUP_DAY);
        String bdate = backupJson.getString(JSON_BACKUP_DATE);
        Date backupDate = dateFormatter.parse(bdate);
        boolean bschedule = backupJson.getBoolean(JSON_BACKUP_SCHEDULE);
        BackupObject bobject = new BackupObject(blocation,bday,backupDate);
        bobject.setSchedule(bschedule);
        data.setBackupDetail(bobject);
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    /**
     * This method will save the data to default location maintain by
     * FilePathManager
     *
     * @param data
     * @throws IOException
     */
    public void saveData(PasswordManagerData data) throws IOException {
        //MAKE SURE DEFAULT SAVE DIR EXIST 
        //IF NOT THEN CREATE IT
        File file = new File(path.getDataPath());
        if (!file.exists()) {
            if (!file.mkdirs()) {
                //EEROR HAPPEN DURING THE FILE CREATION
                throw new IOException("error happen while creating file");
            }
        }

        //DEFAUL FILE PATH AND NAME
        String filePath = path.getDataPath() + JSON_FILE_NAME + JSON_FILE_EXTENSION;
        saveData(data, filePath);
    }

    /**
     * This method will save the data to given location maintain by
     * FilePathManager
     *
     * @param data
     */
    public void saveData(PasswordManagerData data, String location) throws IOException {

        //BUILD THE JSON OBJECTS TO SAVE
        JsonArrayBuilder loginsArrayBuilder = Json.createArrayBuilder();
        ObservableList<Login> logins = data.getLogins();
        User user = data.getUser();
        BackupObject backup = data.getBackupDetail();

        for (Login login : logins) {
            JsonObject loginJson = Json.createObjectBuilder()
                    .add(JSON_LOGIN_NAME, login.getName())
                    .add(JSON_LOGIN_USERID, login.getUserID())
                    .add(JSON_LOGIN_PASSWORD, login.getPassword())
                    .add(JSON_LOGIN_WEBSITE, login.getWebsite())
                    .add(JSON_LOGIN_NOTE, login.getNote())
                    .build();
            loginsArrayBuilder.add(loginJson);
        }
        JsonArray loginsArray = loginsArrayBuilder.build();

        //USER ARRAY CREAION
        JsonObject userlogin = Json.createObjectBuilder()
                .add(JSON_USER_NAME, user.getName())
                .add(JSON_USER_USERID, user.getUserID())
                .add(JSON_USER_PASSWORD, user.getPassword())
                .add(JSON_USER_EMAIL, user.getEmail())
                .add(JSON_USER_RECOVERY, user.getRecovery())
                .build();
        
        //BACKUP DATA CREATION
        JsonObject backupJson = Json.createObjectBuilder()
                .add(JSON_BACKUP_LOCATION, backup.getLocation())
                .add(JSON_BACKUP_DAY ,backup.getDays())
                .add(JSON_BACKUP_DATE, dateFormatter.format(backup.getBackupDate()))
                .add(JSON_BACKUP_SCHEDULE, backup.isSchedule())
                .build();
        
        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_LOGINS, loginsArray)
                .add(JSON_USER, userlogin)
                .add(JSON_BACKUP, backupJson)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        File file = new File(path.getDataPath());
        if (!file.exists()) {
            if (!file.mkdirs()) {
                //EEROR HAPPEN DURING THE FILE CREATION
                throw new IOException("error happen while creating file");
            }
        }
        String filePath = location;
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }
    
    /**
     * This method will export the login detail to csv format at given location.
     * It will replace the new line in LOGIN NOTE to tab.
     * @param selectedFile 
     */
    public void exportData(PasswordManagerData data, File selectedFile) throws IOException {
        //ADD EXTENSION TO SELECTED FILE
        String filePath = selectedFile.getAbsolutePath();
        
        //get all the logins
        ObservableList<Login> logins = data.getLogins();
        
        FileWriter writer = new FileWriter(filePath,false);
        
        for (Login l : logins){
            writer.append(l.getName());
            writer.append(CSV_SEPERATOR);
            writer.append(l.getUserID());
            writer.append(CSV_SEPERATOR);
            writer.append(l.getPassword());
            writer.append(CSV_SEPERATOR);
            writer.append(l.getWebsite());
            writer.append(CSV_SEPERATOR);
            //NEW LINE OF NOTE IS BEING REPLACED TO TAB.
            String note = l.getNote().replaceAll("\n", "\t");
            writer.append(note);
            writer.append("\n");
        }        
        writer.flush();
        writer.close();
    }
    
    /**
     * This will import the data to app. 
     * It requires csv file and exact copy of format of export.
     * @param data
     * @param selectedFile
     * @throws IOException 
     */
    public void importData(PasswordManagerData data, File selectedFile) throws Exception{
        BufferedReader reader = new BufferedReader(new FileReader(selectedFile));
        String line,website,note,name,userID,password; 
        while((line = reader.readLine()) != null){
            String [] l = line.split(CSV_SEPERATOR);
            
            name = l[0];
            userID = l[1];
            password = l[2];
            try{
            website = l[3];
            note = l[4];
            }catch(IndexOutOfBoundsException e){
                //ONE OF THE ITEM IS MISSING
                website  = "";
                note = "";
            }
            if (note != null){
                note.replaceAll("\t", "\n");
            }
            data.addNonDuplicateLogin(name, userID, password, website, note);
        }
    }
    
    /**
     * This will backup the entire data to given file. User need to give file extension. 
     * @param data
     * @param selectedFile
     * @throws IOException 
     */
    public void backupData(PasswordManagerData data, File selectedFile) throws IOException{
        //ADD EXTENSION TO SELECTED FILE
        String filePath = selectedFile.getAbsolutePath();
        saveData(data,filePath);
    }
    
    /**
     * This method restore the data from backup that previously created. 
     * This method assumes that it is being loaded from created backup. 
     * User need to give file extension. 
     * @param data
     * @param selectedFile
     * @throws Exception
     */
    public void restoreData(PasswordManagerData data, File selectedFile) throws Exception{
        //ADD EXTENSION TO SELECTED FILE
        String filePath = selectedFile.getAbsolutePath();
        loadData(data,filePath);
    }
}
