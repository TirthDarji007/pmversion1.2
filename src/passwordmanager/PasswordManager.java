/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import passwordmanager.dataclasses.PasswordManagerData;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import passwordmanager.dataclasses.BackupObject;
import passwordmanager.dataclasses.Login;
import passwordmanager.dataclasses.User;
import passwordmanager.dialogs.AddDialogSingleton;
import passwordmanager.dialogs.AppMessageDialogSingleton;
import passwordmanager.dialogs.LoginDialogSingleton;
import passwordmanager.dialogs.NewUserDialogSingleton;
import passwordmanager.dialogs.ViewDialogSingleton;
import passwordmanager.dialogs.YesNoLabelSingleton;
import passwordmanager.file.PasswordManagerFiles;
import passwordmanager.workspace.PasswordManagerController;
import passwordmanager.workspace.PasswordManagerGUI;
import passwordmanager.workspace.PasswordManagerUserLogin;

/**
 *
 * @author tirth
 * @Version 1.0
 */
public class PasswordManager extends Application {
    Stage stage;
    PasswordManagerGUI gui;
    PasswordManagerData data;
    PasswordManagerController controller;
    PasswordManagerFiles dataManager;
    PasswordManagerUserLogin userLogin;
    
    //DIALOGS
    AddDialogSingleton addDialog;
    ViewDialogSingleton viewDialog;
    AppMessageDialogSingleton messageDialog;
    YesNoLabelSingleton conformationDialog;
    NewUserDialogSingleton newUserDialog;
    LoginDialogSingleton loginDialog;
    
    //VARIABLE THAT HOLD IF IT IS A NEW USER OR RETURNING USER
    boolean newuser = false;
    
    //VARIABLE THAT CONTROLE THE USER IS LOGGED IN OR NOT
    boolean userLogedIn = false;
    
    //SAMPLE LOGIN TO SHOW WHEN NEW USER LOGS IN
    //MUST BE ADDED OTHERWISE SAVE WILL NOT WORK
    Login sampleLogin = new Login("Google","SampleID","newPassword","www.google.com","Look at this Note");
    
    //SAMPLE USER IF NEEDED IN FUTURE
    User sampleUser = new User("admin","admin","admin");
    
    @Override
    public void start(Stage primaryStage) {
        //SAVE THE STAGE
        this.stage = primaryStage;
        
        //INTIATE THE DIALOGS
        addDialog = AddDialogSingleton.getSingleton();
        addDialog.init(stage);
        viewDialog = ViewDialogSingleton.getSingleton();
        viewDialog.init(stage);
        messageDialog = AppMessageDialogSingleton.getSingleton();
        messageDialog.init(stage);
        conformationDialog = YesNoLabelSingleton.getSingleton();
        conformationDialog.init(stage);
        newUserDialog = NewUserDialogSingleton.getSingleton();
        newUserDialog.init(stage);
        loginDialog = LoginDialogSingleton.getSingleton();
        loginDialog.init(stage);
        
        //MAKE THE APP
        //STEP OF THIS PROCESS IS DEPENDENT. 
        //CHANGING WILL RESULT IN BUGGY PROGRAM. 
        data = new PasswordManagerData(this);
        controller = new PasswordManagerController(this);
        dataManager = new PasswordManagerFiles(this);
        initData();
        userLogin = new PasswordManagerUserLogin(this);
        
        //IF USER HAS SUCESSFULLY LOGED IN LOAD THE WORKSPACE AND DISPLAY APP
        if(userLogedIn){
            //MAKE THE WORKSPACE
            gui = new PasswordManagerGUI(this,primaryStage);
        
            //INITE APP CONTROLS
            stage.setOnCloseRequest(e->{
                handleCloseRequest();
            });
        
            stage.setTitle("Password Manager");
            stage.show();
        }
    }

    public Stage getStage() {
        return stage;
    }

    public PasswordManagerGUI getGui() {
        return gui;
    }

    public PasswordManagerData getData() {
        return data;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setGui(PasswordManagerGUI gui) {
        this.gui = gui;
    }

    public void setData(PasswordManagerData data) {
        this.data = data;
    }

    public PasswordManagerController getController() {
        return controller;
    }

    public void setController(PasswordManagerController controller) {
        this.controller = controller;
    }

    public AddDialogSingleton getAddDialog() {
        return addDialog;
    }

    public ViewDialogSingleton getViewDialog() {
        return viewDialog;
    }

    public AppMessageDialogSingleton getMessageDialog() {
        return messageDialog;
    }

    public YesNoLabelSingleton getConformationDialog() {
        return conformationDialog;
    }

    public NewUserDialogSingleton getNewUserDialog() {
        return newUserDialog;
    }

    public LoginDialogSingleton getLoginDialog() {
        return loginDialog;
    }

    public boolean isNewuser() {
        return newuser;
    }

    public void setUserLogedIn(boolean userLogedIn) {
        this.userLogedIn = userLogedIn;
    }    

    public PasswordManagerFiles getDataManager() {
        return dataManager;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    //HELPERS 
    
    /**
     * THIS METHOD WILL BE CALLED WHEN APP IS CLOSED
     */
    private void handleCloseRequest() {
        //SAVE THE DATA BEFORE CLOSING THE APP
        try {
            //ADD SAMPLE LOGIN IF DATA IS EMPTY SO SAVE WILL WORK WITOUT ERROR
            if(data.getLogins().isEmpty()){data.addLogin(sampleLogin);}
            
            //ADD SAMPLE USER IF NO USER IS ADDED. THIS STEP SHOULD NOT EXECUTED IN FINAL APP.
            if(data.getUser() == null){data.setUser(sampleUser);}
            
            
            //IF WE NEED TO BACKUP THEN BACKUP            
            if(data.getBackupDetail().isSchedule() && data.getBackupDetail().isBackupDue(Calendar.getInstance().getTime())){
                dataManager.backupData(data, new File(data.getBackupDetail().getLocation() + File.separatorChar + "AutoBackup.pmbkp"));
                data.getBackupDetail().setBackupDate(Calendar.getInstance().getTime());
            }
            
            dataManager.saveData(data);            
        } catch (IOException ex) {
            messageDialog.show("Error", "Error happen while saving the data");
            ex.printStackTrace();
        }
        catch (Exception ex) {
            messageDialog.show("Error", "Error happen while saving the data");
            ex.printStackTrace();
        }
    }
    
    /**
     * THIS METHOD LOADS THE DATA AND SETSUP INITIAL VARIABLE TO CONTINUE 
     * THE APP EXECUTION.
     */
    private void initData() {
        
        //TRY TO LOAD DATA
        try {
            dataManager.loadData(data);
        } catch (Exception ex) {
            ex.printStackTrace();
            //EROR IN LOADING DATA MEANS DATA FILE DOES NOT EXIST
            //SET NEW USER TRUE
            newuser = true;
            
            //ADD SAMPLE LOGIN TO DATA
            data.addLogin(sampleLogin);
        }
    }
    
}
