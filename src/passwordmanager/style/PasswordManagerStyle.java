/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.style;

/**
 *
 * @author tirth
 */
public class PasswordManagerStyle {
    // WE'LL USE THIS FOR ORGANIZING WELCOME SCREEN AND APPLICATION TEXT
    public static String CLASS_BORDER_PANE = "border_pane";
    public static String CLASS_TEXT = "regular_text";
    public static String CLASS_TEXT_FIELD = "text_field";
    public static String CLASS_BUTTON = "button";
    public static String CLASS_STEP_LABEL = "step_label";
    public static String CLASS_STEP_HEADER = "step_header";
    public static String CLASS_SIDEMENU_BUTTON = "sidemenu_button_style";
    public static String CLASS_SIDEMENU_BUTTON_COLOR = "sidemenu_button_color";
    public static String CLASS_SIDEMENU_SELECTED_BUTTON = "sidemenu_selected_button";
    public static String CLASS_LOGIN_TABLE ="login_table";
    public static String CLASS_SIDEMENU_PANE ="sidemenu_style";
    public static String CLASS_PASSWORD_PANE ="passwordpane_style";
    public static String CLASS_USER_LABEL = "user_label";
}
