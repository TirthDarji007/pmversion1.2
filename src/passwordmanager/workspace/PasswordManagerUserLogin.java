/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.workspace;

import passwordmanager.PasswordManager;

/**
 *
 * @author tirth
 */
public class PasswordManagerUserLogin {
    PasswordManager app;

    public PasswordManagerUserLogin(PasswordManager aThis) {
        this.app = aThis;
        
        //IF THE USER IS NEW GO FOR NEW USER SETUP ELSE USE LOGIN
        if (app.isNewuser()){
            newUserSetup();
        }
        else{
            returnUser();
        }
    }

    private void newUserSetup() {
        //OPEN THE DIALOG FOR NEW USER SETUP
        app.getNewUserDialog().display();
        
        //SAVE THE USER DATA
        app.getData().setUser(app.getNewUserDialog().getResponse());
        
        //ACTIVATE THE USER
        app.setUserLogedIn(true);
    }

    private void returnUser() {
        //OPEN THE DIALOG TO LOGIN
        //GET THE USE FROM LOADED DATA FROM FILE
        app.getLoginDialog().display(app.getData().getUser());
        
        //SET THE USER RESPONSE INTO HANLER VARIABLE
        app.setUserLogedIn(app.getLoginDialog().getResponse());
    }
    
}
