/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.workspace;

import passwordmanager.dataclasses.Login;
import java.net.URL;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import passwordmanager.PasswordManager;
import static passwordmanager.style.PasswordManagerStyle.*;

/**
 *
 * @author tirth
 */
public class PasswordManagerGUI {

    PasswordManager app;
    Stage stage;

    //CONTROLLER FOR APP
    PasswordManagerController controller;

    //INT TO MANAGE STAGE 
    int step = 1;

    //SIDE MENU BUTTON & CONTENTS
    Label stepInfo;
    Button passwordButton;
    Button userButton;
    Button impoExpoButton;
    Button backupRestoreButton;
    Button licenseButton;
    Button helpButton;

    //PASSWORD TOOLBAR
    Button addButton;
    Button editButton;
    Button removeButton;
    Button viewButton;
    Button websiteButton;

    // FOR THE LOGIN TABLE
    ScrollPane loginsTableScrollPane;
    TableView<Login> loginsTableView;
    TableColumn<Login, StringProperty> nameColumn;
    TableColumn<Login, StringProperty> userIDColumn;
    TableColumn<Login, StringProperty> passwordColumn;
    TableColumn<Login, StringProperty> websiteColumn;

    //CONTAINERS FOR MAIN GUI
    VBox sideMenu;
    FlowPane passwordToolbar;

    //5 DIFFERENT PANE FOR 5 DIFFRENT MENU OPTION 
    BorderPane passwordPane;
    BorderPane userPane;
    BorderPane impoExpoPane;
    BorderPane backupPane;
    BorderPane licensePane;
    BorderPane helpPane;

    //FINAL PANE
    Scene scene;
    BorderPane pane;

    public PasswordManagerGUI(PasswordManager app, Stage primaryStage) {
        this.app = app;
        this.stage = primaryStage;
        controller = app.getController();

        //INITIATE THE COMPONENT
        initVariables();

        //RENDER GUI 
        renderGUI();

        //INITIATE CONTROLLERS
        initControls();

        //INITIATE STYLE
        initStyle();

        //DISABLE THE PASSWORD TOOLBAR AND MAKE ADD BUTTON ENABLE
        disablePasswordToolbar(true);
        addButton.setDisable(false);

        //LOAD ADDISIONAL PANE FROM OTHER CLASSES
        initExtPane();
    }

    private void initVariables() {

        //SIDE MENU BUTTONS
        stepInfo = new Label("Password Managment");
        passwordButton = new Button("Passwords");
        userButton = new Button("User Option");
        impoExpoButton = new Button("Import & Export");
        backupRestoreButton = new Button("Backup & Restore");
        licenseButton = new Button("License");
        helpButton = new Button("Help");
        passwordButton.alignmentProperty().setValue(Pos.CENTER_LEFT);
        userButton.alignmentProperty().setValue(Pos.CENTER_LEFT);
        impoExpoButton.alignmentProperty().setValue(Pos.CENTER_LEFT);
        backupRestoreButton.alignmentProperty().setValue(Pos.CENTER_LEFT);
        helpButton.alignmentProperty().setValue(Pos.CENTER_LEFT);
        licenseButton.alignmentProperty().setValue(Pos.CENTER_LEFT);

        //INITIATE LOGIN TABLE
        loginsTableScrollPane = new ScrollPane();
        loginsTableView = new TableView();
        nameColumn = new TableColumn("Name");
        userIDColumn = new TableColumn("UserID");
        passwordColumn = new TableColumn("Password");
        websiteColumn = new TableColumn("Website");
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<Login, StringProperty>("name")
        );
        userIDColumn.setCellValueFactory(
                new PropertyValueFactory<Login, StringProperty>("userID")
        );
        passwordColumn.setCellValueFactory(
                new PropertyValueFactory<Login, StringProperty>("password")
        );
        websiteColumn.setCellValueFactory(
                new PropertyValueFactory<Login, StringProperty>("website")
        );

        //CREATING PASSWORD TOOLBAR
        addButton = new Button("Add");
        removeButton = new Button("Remove");
        editButton = new Button("Edit");
        viewButton = new Button("View");
        websiteButton = new Button("Go to Website");
        addButton.setTooltip(new Tooltip("Add new Password Information"));
        removeButton.setTooltip(new Tooltip("Remove Selected Password Information"));
        editButton.setTooltip(new Tooltip("Edit Selected Password Information"));
        viewButton.setTooltip(new Tooltip("View Selected Password Information"));
        websiteButton.setTooltip(new Tooltip("Open Website in Deafult Browser"));

        //CONTAINER FOR GUI
        sideMenu = new VBox();
        passwordToolbar = new FlowPane();

        //5 DIFFERENT PANE FOR 5 DIFFRENT MENU OPTION 
        passwordPane = new BorderPane();
        userPane = new BorderPane();
        impoExpoPane = new BorderPane();
        backupPane = new BorderPane();
        licensePane = new BorderPane();
        helpPane = new BorderPane();

        //FINAL PANE
        pane = new BorderPane();
    }

    private void renderGUI() {

        //MAKE SIDE MENU
        sideMenu.getChildren().add(stepInfo);
        sideMenu.getChildren().add(passwordButton);
        sideMenu.getChildren().add(userButton);
        sideMenu.getChildren().add(impoExpoButton);
        sideMenu.getChildren().add(backupRestoreButton);
        //sideMenu.getChildren().add(licenseButton);
        sideMenu.getChildren().add(helpButton);
        sideMenu.setFillWidth(true);
        sideMenu.setPrefWidth(250);
        sideMenu.setSpacing(5);
        sideMenu.setPadding(new Insets(10, 0, 10, 0));
        passwordButton.prefWidthProperty().bind(sideMenu.widthProperty());
        userButton.prefWidthProperty().bind(sideMenu.widthProperty());
        impoExpoButton.prefWidthProperty().bind(sideMenu.widthProperty());
        backupRestoreButton.prefWidthProperty().bind(sideMenu.widthProperty());
        licenseButton.prefWidthProperty().bind(sideMenu.widthProperty());
        helpButton.prefWidthProperty().bind(sideMenu.widthProperty());

        //MAKE THE LOGIN TABLE
        loginsTableView.getColumns().add(nameColumn);
        loginsTableView.getColumns().add(userIDColumn);
        loginsTableView.getColumns().add(passwordColumn);
        loginsTableView.getColumns().add(websiteColumn);
        nameColumn.prefWidthProperty().bind(loginsTableView.widthProperty().divide(4));
        userIDColumn.prefWidthProperty().bind(loginsTableView.widthProperty().divide(4));
        passwordColumn.prefWidthProperty().bind(loginsTableView.widthProperty().divide(4));
        websiteColumn.prefWidthProperty().bind(loginsTableView.widthProperty().divide(4));
        loginsTableView.setItems(app.getData().getLogins());

        //RENDERING PASSWORD TOOLBAR
        passwordToolbar.getChildren().add(addButton);
        passwordToolbar.getChildren().add(removeButton);
        passwordToolbar.getChildren().add(editButton);
        passwordToolbar.getChildren().add(viewButton);
        passwordToolbar.getChildren().add(websiteButton);
        passwordToolbar.setHgap(10);
        passwordToolbar.setVgap(10);
        passwordToolbar.setPadding(new Insets(10, 10, 10, 0));

        //RENDERING PASSWORD PANE
        loginsTableScrollPane.setContent(loginsTableView);
        loginsTableScrollPane.setFitToWidth(true);
        loginsTableScrollPane.setFitToHeight(true);
        loginsTableScrollPane.prefWidthProperty().bind(passwordPane.widthProperty());
        passwordPane.setTop(passwordToolbar);
        passwordPane.setCenter(loginsTableScrollPane);

        //FINAL PANE
        pane.setLeft(sideMenu);
        pane.setCenter(passwordPane);
        pane.prefWidthProperty().bind(stage.widthProperty());
        scene = new Scene(pane, 1280, 700);
        stage.setScene(scene);
    }

    private void initControls() {
        //SIDE MENU BUTTON HANDLER
        passwordButton.setOnAction(e -> {
            controller.handlePasswordButton();
        });
        userButton.setOnAction(e -> {
            controller.handleUserbutton();
        });
        impoExpoButton.setOnAction(e -> {
            controller.handleImpoExpoButton();
        });
        backupRestoreButton.setOnAction(e -> {
            controller.handleBackupRestoreButton();
        });
        licenseButton.setOnAction(e -> {
            controller.handleLicenseButton();
        });
        helpButton.setOnAction(e -> {
            controller.handleHelpButton();
        });

        //PASSWORD PANE CONTROLLERS 
        addButton.setOnAction(e -> {
            controller.handleAddButton();
        });
        removeButton.setOnAction(e -> {
            controller.handleRemoveButton();
        });
        editButton.setOnAction(e -> {
            controller.handleEditButton();
        });
        viewButton.setOnAction(e -> {
            controller.handleViewButton();
        });
        websiteButton.setOnAction(e -> {
            controller.handleWebsiteButton();
        });
        loginsTableView.getSelectionModel().selectedItemProperty().addListener(e -> {
            controller.handleSlideSelected();
        });
        loginsTableView.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                controller.handleEditButton();
            }
        });
    }

    private void initStyle() {
        //add stylesheet to application
        String stylesheet = "style/";
        stylesheet += "pmStyle.css";
        Class appClass = app.getClass();
        URL stylesheetURL = appClass.getResource(stylesheet);
        String stylesheetPath = stylesheetURL.toExternalForm();
        scene.getStylesheets().add(stylesheetPath);

        //SIDE MENU STYLE
        sideMenu.getStyleClass().add(CLASS_SIDEMENU_PANE);
        stepInfo.getStyleClass().add(CLASS_STEP_LABEL);
        passwordButton.getStyleClass().add(CLASS_SIDEMENU_BUTTON);
        userButton.getStyleClass().add(CLASS_SIDEMENU_BUTTON);
        impoExpoButton.getStyleClass().add(CLASS_SIDEMENU_BUTTON);
        backupRestoreButton.getStyleClass().add(CLASS_SIDEMENU_BUTTON);
        licenseButton.getStyleClass().add(CLASS_SIDEMENU_BUTTON);
        helpButton.getStyleClass().add(CLASS_SIDEMENU_BUTTON);
        passwordButton.getStyleClass().add(CLASS_SIDEMENU_SELECTED_BUTTON);
        loginsTableView.getStyleClass().add(CLASS_LOGIN_TABLE);

        //PASSWORD PANE STYLE
        passwordPane.getStyleClass().add(CLASS_PASSWORD_PANE);
    }

    private void initExtPane() {
        //USER OPTION PANE
        PMUserOptionGUI userOptionPane = new PMUserOptionGUI(app);
        this.userPane = userOptionPane.getUserOptionGUI();

        //IMPORT EXPORT PANE
        PMImpoExpoGUI impoExpoPaneGUI = new PMImpoExpoGUI(app);
        this.impoExpoPane = impoExpoPaneGUI.getImpoExpoGUI();

        //BACKUP & RESTORE PANE
        PMBackupGUI backupPaneGUI = new PMBackupGUI(app);
        this.backupPane = backupPaneGUI.getBackupGUI();
        
        //HELP PANE
        PMHelpGUI helpPaneGUI = new PMHelpGUI(app);
        this.helpPane = helpPaneGUI.getHelpGUI();
    }

    //HELPER METHODS 
    /**
     * This method removes the selected style from the selected menu button
     */
    protected void resetSelectedMenuButton() {
        if (step > 0 && step < 7) {
            switch (step) {
                case 1:
                    passwordButton.getStyleClass().remove(CLASS_SIDEMENU_SELECTED_BUTTON);
                    break;
                case 2:
                    userButton.getStyleClass().remove(CLASS_SIDEMENU_SELECTED_BUTTON);
                    break;
                case 3:
                    impoExpoButton.getStyleClass().remove(CLASS_SIDEMENU_SELECTED_BUTTON);
                    break;
                case 4:
                    backupRestoreButton.getStyleClass().remove(CLASS_SIDEMENU_SELECTED_BUTTON);
                    break;
                case 5:
                    licenseButton.getStyleClass().remove(CLASS_SIDEMENU_SELECTED_BUTTON);
                    break;
                case 6:
                    helpButton.getStyleClass().remove(CLASS_SIDEMENU_SELECTED_BUTTON);
                    break;
            }
        }
    }
    
    /**
     * This method will reload the user setting pane.
     * Restore will use this to reload updated data to workspace.
     */
    protected void reloadUserOptionGUI(){
        PMUserOptionGUI userOptionPane = new PMUserOptionGUI(app);
        userPane = userOptionPane.getUserOptionGUI();
    }
    
    /**
     * This method will reload the backup pane.
     * Restore will use this to reload updated data to workspace.
     */
    protected void reloadBackupGUI(){
        PMBackupGUI backupPaneGUI = new PMBackupGUI(app);
        backupPane = backupPaneGUI.getBackupGUI();
    }

    /**
     * THIS METHOD DISABLE THE SIDE MENU OF APPLICATION. IT DOES NOT DISABLE THE
     * LICENSE BUTTON
     *
     * @param value
     */
    protected void diableSideMenu(boolean value) {
        passwordButton.setDisable(value);
        userButton.setDisable(value);
        impoExpoButton.setDisable(value);
        backupRestoreButton.setDisable(value);
        helpButton.setDisable(value);
    }

    /**
     * THIS METHOD DISABLE THE PASSWORD TOOLBAR BUTTONS
     *
     * @param value
     */
    protected void disablePasswordToolbar(boolean value) {
        addButton.setDisable(value);
        removeButton.setDisable(value);
        editButton.setDisable(value);
        viewButton.setDisable(value);
        websiteButton.setDisable(value);
    }

    /**
     * THIS METHOD WILL MAKE CHANGES TO WORKSPACE ACCORDING VALUE GIVEN. 
     * TRUE = ENABLE CONTROLS WHEN SINGLE SLIDE IS SELECTED. 
     * FALSE = DISABLE CONTROLS WHEN NO SLIDE IS SELECTED.
     *
     * @param value
     */
    protected void singleLoginSelected(boolean value) {
        removeButton.setDisable(!value);
        editButton.setDisable(!value);
        viewButton.setDisable(!value);
        websiteButton.setDisable(!value);
    }
}
