/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.workspace;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import passwordmanager.PasswordManager;
import passwordmanager.dataclasses.User;
import static passwordmanager.style.PasswordManagerStyle.*;

/**
 *
 * @author tirth
 */
public class PMUserOptionGUI {

    PasswordManager app;
    PasswordManagerController controller;

    //COMPONENT FOR USER PANE
    Label topLabel;
    Label infoLabel1;
    Label infoLabel2;
    Label infoLabel3;
    Label infoLabel4;
    Label nameLabel;
    Label userIDLabel;
    Label passwordLabel;
    Label emailLabel;
    Label recoveryLabel;
    TextField nameText;
    TextField userIDText;
    TextField passwordText;
    TextField emailText;
    TextField recoveryText;
    Button submitButton;
    GridPane form;
    VBox pane;

    //FINAL PANE
    BorderPane finalPane;

    /**
     * This class creates the Pane for User Option for Password Manager App.
     * This will be called from PasswordManagerGUI class. This app will
     * initialize the pane, render GUI, initialize controls and style them.
     *
     * @param app
     */
    public PMUserOptionGUI(PasswordManager app) {
        this.app = app;

        controller = app.getController();

        //INITIATE THE COMPONENT
        initVariables();

        //RENDER GUI 
        renderGUI();

        //INITIATE CONTROLLERS
        initControls();

        //INITIATE STYLE
        initStyle();
    }

    /**
     * This method will provide the pane needed for user option.
     *
     * @return
     */
    public BorderPane getUserOptionGUI() {
        return finalPane;
    }

    private void initVariables() {
        //INIT LABELS
        topLabel = new Label("User Login Settings");
        infoLabel1 = new Label("Please enter all the information to keep your profile up to date.");
        infoLabel2 = new Label("UserID, Password and Recovery Code is required.");
        infoLabel3 = new Label("Recovery Code will be used as alternative to login, if you have forgotten the password.");
        infoLabel4 = new Label("To make changes enter the new Information and click Submit button.");
        nameLabel = new Label("Name:");
        userIDLabel = new Label("UserID:");
        passwordLabel = new Label("Password:");
        emailLabel = new Label("Email:");
        recoveryLabel = new Label("Recovery Code:");

        //INIT TEXTFIELDS
        nameText = new TextField();
        userIDText = new TextField();
        passwordText = new TextField();
        emailText = new TextField();
        recoveryText = new TextField();
        submitButton = new Button("Submit");
        submitButton.setDisable(true);

        User user = app.getData().getUser();
        setText(user);

        //INIT PANE
        form = new GridPane();
        pane = new VBox();
        finalPane = new BorderPane();
    }

    private void renderGUI() {
        //MAKING THE FORM
        form.add(nameLabel, 0, 0);
        form.add(nameText, 1, 0);
        form.add(userIDLabel, 0, 1);
        form.add(userIDText, 1, 1);
        form.add(passwordLabel, 0, 2);
        form.add(passwordText, 1, 2);
        form.add(emailLabel, 0, 3);
        form.add(emailText, 1, 3);
        form.add(recoveryLabel, 0, 4);
        form.add(recoveryText, 1, 4);
        form.setHgap(10);
        form.setVgap(10);
        GridPane.setFillWidth(form, true);
        GridPane.setFillHeight(form, true);
        nameLabel.prefWidthProperty().bind(form.widthProperty().divide(3));
        nameText.prefWidthProperty().bind(form.widthProperty());
        form.prefWidthProperty().bind(pane.widthProperty());

        //MAKING PANE
        pane.getChildren().add(infoLabel1);
        pane.getChildren().add(infoLabel2);
        pane.getChildren().add(infoLabel3);
        pane.getChildren().add(infoLabel4);
        pane.getChildren().add(form);
        pane.getChildren().add(submitButton);
        pane.setSpacing(20);
        pane.setFillWidth(true);
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.prefWidthProperty().bind(finalPane.widthProperty());
        pane.prefHeightProperty().bind(finalPane.heightProperty());
        topLabel.prefWidthProperty().bind(finalPane.widthProperty());
        topLabel.setAlignment(Pos.CENTER);

        //MAKING FINAL PANE
        finalPane.setTop(topLabel);
        finalPane.setCenter(pane);
    }

    private void initControls() {
        submitButton.setOnAction(e -> {
            handleSubmitButton();
        });
        finalPane.setOnKeyPressed(e -> {
            if ((e.getCode() == KeyCode.ENTER) && (!submitButton.isDisable())) {
                handleSubmitButton();
            }
        });
        nameText.textProperty().addListener((observable, oldValue, newValue) -> {
            eableSubmitButton();
        });
        userIDText.textProperty().addListener((observable, oldValue, newValue) -> {
            eableSubmitButton();
        });
        passwordText.textProperty().addListener((observable, oldValue, newValue) -> {
            eableSubmitButton();
        });
        emailText.textProperty().addListener((observable, oldValue, newValue) -> {
            eableSubmitButton();
        });
        recoveryText.textProperty().addListener((observable, oldValue, newValue) -> {
            eableSubmitButton();
        });
    }

    private void initStyle() {
        //ADD THE TEXT STYLE
        pane.getStyleClass().add(CLASS_TEXT);

        //LABEL
        infoLabel1.getStyleClass().add(CLASS_USER_LABEL);
        infoLabel2.getStyleClass().add(CLASS_USER_LABEL);
        infoLabel3.getStyleClass().add(CLASS_USER_LABEL);
        infoLabel4.getStyleClass().add(CLASS_USER_LABEL);

        /* //UNCOMENT IT TO CHANGE FOEM LABEL TEXT TO BOLD
        nameLabel.getStyleClass().add(CLASS_USER_LABEL);
        userIDLabel.getStyleClass().add(CLASS_USER_LABEL);
        passwordLabel.getStyleClass().add(CLASS_USER_LABEL);
        emailLabel.getStyleClass().add(CLASS_USER_LABEL);
        recoveryLabel.getStyleClass().add(CLASS_USER_LABEL);
         */
        //OTHER STYLE
        submitButton.getStyleClass().add(CLASS_BUTTON);
        topLabel.getStyleClass().add(CLASS_STEP_HEADER);
    }

    /**
     * THIS METHOD WILL ENABLE THE ENTER BUTTON IF NAME, USERID AND PASSWORD IS
     * ENTERED.
     */
    private void eableSubmitButton() {
        if ((!userIDText.getText().isEmpty())
                && (!passwordText.getText().isEmpty())
                && (!recoveryText.getText().isEmpty())) {

            submitButton.setDisable(false);

        } else {
            submitButton.setDisable(true);
        }
    }

    private void setText(User user) {
        nameText.setText(user.getName());
        userIDText.setText(user.getUserID());
        passwordText.setText(user.getPassword());
        emailText.setText(user.getEmail());
        recoveryText.setText(user.getRecovery());
    }

    private void handleSubmitButton() {
        User user = new User(userIDText.getText(), passwordText.getText(), recoveryText.getText());
        user.setName(nameText.getText());
        user.setEmail(emailText.getText());
        app.getData().setUser(user);
        submitButton.setDisable(true);

        //IMFORM USER THAT USER IS UPDATED
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Update");
        alert.setHeaderText(null);
        alert.setContentText("Login detail is updated.");
        alert.showAndWait();
    }
}
