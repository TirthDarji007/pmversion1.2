/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.workspace;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import passwordmanager.PasswordManager;
import static passwordmanager.style.PasswordManagerStyle.*;

/**
 *
 * @author tirth
 */
public class PMImpoExpoGUI {

    //SAVE THE APP
    PasswordManager app;
    PasswordManagerController controller;

    //COMPONENT FOR USER PANE
    Label topLabel;
    Label infoLabel1;
    Label infoLabel2;
    Label infoLabel3;
    Label infoLabel4;
    Label infoLabel5;
    Label importLabel;
    Label exportLabel;
    Button importButton;
    Button exportButton;
    VBox pane;
    GridPane form;

    //FINAL PANE
    BorderPane finalPane;

    public PMImpoExpoGUI(PasswordManager app) {
        this.app = app;

        controller = app.getController();

        //INITIATE THE COMPONENT
        initVariables();

        //RENDER GUI 
        renderGUI();

        //INITIATE CONTROLLERS
        initControls();

        //INITIATE STYLE
        initStyle();
    }

    /**
     * This method will provide the pane needed for import & export pane.
     *
     * @return
     */
    public BorderPane getImpoExpoGUI() {
        return finalPane;
    }

    private void initVariables() {
        //INIT LABELS
        topLabel = new Label("Import & Export Wizard");
        infoLabel1 = new Label("Welcome to Import & Export Wizard.");
        infoLabel2 = new Label("Read following detail before importing or exporting data.");
        infoLabel3 = new Label("Import will import login detail from selected csv file.");
        infoLabel4 = new Label("Export will export all login detail to csv file.");
        infoLabel5 = new Label("Refer to Help for formatting infomation of csv file.");
        importLabel = new Label("Start Import");
        exportLabel = new Label("Start Export");
        importButton = new Button("Import");
        exportButton = new Button("Export");

        //INIT PANES
        pane = new VBox(25);
        form = new GridPane();
        finalPane = new BorderPane();
    }

    private void renderGUI() {
        //MAKING FORM
        form.add(importLabel, 0, 0);
        form.add(importButton, 1, 0);
        form.add(exportLabel, 0, 1);
        form.add(exportButton, 1, 1);

        //MAKING PANE
        pane.getChildren().add(infoLabel1);
        pane.getChildren().add(infoLabel2);
        pane.getChildren().add(infoLabel3);
        pane.getChildren().add(infoLabel4);
        pane.getChildren().add(infoLabel5);
        pane.getChildren().add(form);

        //FINAL PANE
        finalPane.setTop(topLabel);
        finalPane.setCenter(pane);

        //SIZE THE GUI
        pane.prefWidthProperty().bind(finalPane.widthProperty());
        pane.prefHeightProperty().bind(finalPane.heightProperty());
        pane.setFillWidth(true);
        pane.setPadding(new Insets(10, 10, 10, 10));
        form.setHgap(50);
        form.setVgap(50);
        form.setPadding(new Insets(40, 0, 0, 0));
        topLabel.prefWidthProperty().bind(finalPane.widthProperty());
        topLabel.setAlignment(Pos.CENTER);
    }

    private void initControls() {
        importButton.setOnAction(e -> {
            controller.handleImportRequest();
        });
        exportButton.setOnAction(e -> {
            controller.handleExportRequest();
        });
    }

    private void initStyle() {
        //ADD THE TEXT STYLE
        pane.getStyleClass().add(CLASS_TEXT);

        //LABEL
        infoLabel1.getStyleClass().add(CLASS_USER_LABEL);
        infoLabel2.getStyleClass().add(CLASS_USER_LABEL);
        infoLabel3.getStyleClass().add(CLASS_USER_LABEL);
        infoLabel4.getStyleClass().add(CLASS_USER_LABEL);
        infoLabel5.getStyleClass().add(CLASS_USER_LABEL);
        importLabel.getStyleClass().add(CLASS_USER_LABEL);
        exportLabel.getStyleClass().add(CLASS_USER_LABEL);

        //BUTTON 
        importButton.getStyleClass().add(CLASS_BUTTON);
        exportButton.getStyleClass().add(CLASS_BUTTON);

        //HEADER
        topLabel.getStyleClass().add(CLASS_STEP_HEADER);
    }
}
