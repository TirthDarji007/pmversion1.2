/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.workspace;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.FileChooser;
import passwordmanager.PasswordManager;
import passwordmanager.dataclasses.Login;
import passwordmanager.dataclasses.User;
import passwordmanager.file.FilePathManager;
import static passwordmanager.style.PasswordManagerStyle.*;

/**
 *
 * @author tirth
 */
public class PasswordManagerController {

    //SAVE THE APP FOE GETTING ALL THE CONTROLS
    PasswordManager app;
    FilePathManager path = new FilePathManager();

    /**
     * This this the controller for all UI in Password Manager application
     *
     * @param app
     */
    public PasswordManagerController(PasswordManager app) {
        this.app = app;
    }

    /**
     * Controller to handle password button of side menu. Set stage to password
     * manage
     */
    public void handlePasswordButton() {
        PasswordManagerGUI workspace = app.getGui();

        //SET THE WORKSPACE 
        workspace.pane.setCenter(workspace.passwordPane);
        workspace.stepInfo.setText("Password Managment");

        //RESET THE COLOR OF SIDE MENU 
        workspace.resetSelectedMenuButton();
        workspace.passwordButton.getStyleClass().add(CLASS_SIDEMENU_SELECTED_BUTTON);

        //UPDATE STEP
        workspace.step = 1;
    }

    /**
     * CONTROLLER HANDLE THE USER OPTION BUTTON OF SIDE MENU. IT SETUPS THE
     * WORKSPACE.
     */
    public void handleUserbutton() {
        PasswordManagerGUI workspace = app.getGui();

        //SET THE WORKSPACE 
        workspace.pane.setCenter(workspace.userPane);
        workspace.stepInfo.setText("Usesr Options");

        //RESET THE COLOR OF SIDE MENU 
        workspace.resetSelectedMenuButton();
        workspace.userButton.getStyleClass().add(CLASS_SIDEMENU_SELECTED_BUTTON);

        //UPDATE STEP
        workspace.step = 2;
    }

    /**
     * CONTROLLER HANLE THE IMPORT AND EXPORT BUTTON OF SIDE MENU. IT SETUPS THE
     * WORKSPACE.
     */
    public void handleImpoExpoButton() {
        PasswordManagerGUI workspace = app.getGui();

        //SET THE WORKSPACE 
        workspace.pane.setCenter(workspace.impoExpoPane);
        workspace.stepInfo.setText("Import & Export");

        //RESET THE COLOR OF SIDE MENU
        workspace.resetSelectedMenuButton();
        workspace.impoExpoButton.getStyleClass().add(CLASS_SIDEMENU_SELECTED_BUTTON);

        //UPDATE STEP
        workspace.step = 3;
    }

    /**
     * CONTROLLER HANDLE THE BACKBUP & RESTORE BUTTON OF SIDE MENU. IT SETUPS
     * THE WORKSPACE.
     */
    public void handleBackupRestoreButton() {
        PasswordManagerGUI workspace = app.getGui();

        //SET THE WORKSPACE 
        workspace.pane.setCenter(workspace.backupPane);
        workspace.stepInfo.setText("Backup & Restore");

        //RESET THE COLOR OF SIDE MENU 
        workspace.resetSelectedMenuButton();
        workspace.backupRestoreButton.getStyleClass().add(CLASS_SIDEMENU_SELECTED_BUTTON);

        //UPDATE STEP
        workspace.step = 4;
    }

    /**
     * CONTROLLER HANDLE THE LICENSE BUTTON OF SIDE MENU. IT SETUPS THE
     * WORKSPACE.
     */
    public void handleLicenseButton() {
        PasswordManagerGUI workspace = app.getGui();

        //SET THE WORKSPACE 
        workspace.pane.setCenter(workspace.licensePane);
        workspace.stepInfo.setText("License");

        //RESET THE COLOR OF SIDE MENU 
        workspace.resetSelectedMenuButton();
        workspace.licenseButton.getStyleClass().add(CLASS_SIDEMENU_SELECTED_BUTTON);

        //UPDATE STEP
        workspace.step = 5;
    }

    /**
     * CONTROLLER HANDLE THE HELP BUTTON OF SIDE MENU. IT SETUPS THE WORKSPACE.
     */
    public void handleHelpButton() {
        PasswordManagerGUI workspace = app.getGui();

        //SET THE WORKSPACE 
        workspace.pane.setCenter(workspace.helpPane);
        workspace.stepInfo.setText("Help");

        //RESET THE COLOR OF SIDE MENU 
        workspace.resetSelectedMenuButton();
        workspace.helpButton.getStyleClass().add(CLASS_SIDEMENU_SELECTED_BUTTON);

        //UPDATE STEP
        workspace.step = 6;
    }

    //PASSWORDPANE CONTROLLERS 
    /**
     * CONTROLLER TO HANDLE ADD BUTTON. ADDS THE NON DUBLICATE LOGIN TO DATA
     */
    public void handleAddButton() {
        //GET THE WORKSPACE
        PasswordManagerGUI workspace = app.getGui();

        //OPEN THE DIALOG FOR LOGIN INPUT
        app.getAddDialog().display();

        //GET THE LOGIN
        Login login = app.getAddDialog().getResponse();
        boolean edited = app.getAddDialog().isIsedited();

        //ADD IT TO THE APP DATA
        if (edited) {
            boolean added = app.getData().addLogin(login);
            if(!added){
                 app.getMessageDialog().show("Login Exist", "Similar Login already Exist. \nClick on Name to sort by Name.");
            }
        }

        //REFRESH THE TABLE
        workspace.loginsTableView.refresh();
    }

    /**
     * CONTROLLER TO HANLE REMOVE BUTTON. REMOVES THE LOGIN FROM DATA
     */
    public void handleRemoveButton() {
        //GET THE WORKSPACE
        PasswordManagerGUI workspace = app.getGui();

        //GET SELECTED LOGIN
        Login login = workspace.loginsTableView.getSelectionModel().getSelectedItem();

        //OPEN THE DIALOG FOR CONFORMATION
        app.getConformationDialog().show("Conformation", "Do you really want to delete the Login?");

        //GET THE RESPONSE OF USER
        boolean response = app.getConformationDialog().getResponse();

        //DELETE LOGIN IF USER RESPONED TO YES
        if (response) {
            app.getData().removeLogin(login);

            //REMOVE THE SELECTION FOR SECURITY
            workspace.loginsTableView.getSelectionModel().clearSelection();
            workspace.singleLoginSelected(false);
        }

        //REFRESH THE TABLE
        workspace.loginsTableView.refresh();

    }

    /**
     * CONTROLLER TO HANDLE EDIT BUTTON MAKES CHANGES TO SELECTED LOGIN
     */
    public void handleEditButton() {
        //GET THE WORKSPACE
        PasswordManagerGUI workspace = app.getGui();

        //GET SELECTED LOGIN
        Login loginold = workspace.loginsTableView.getSelectionModel().getSelectedItem();

        //OPEN THE DIALOG FOR LOGIN EDIT
        app.getAddDialog().display(loginold);

        //GET THE LOGIN
        Login loginNew = app.getAddDialog().getResponse();
        boolean edited = app.getAddDialog().isIsedited();

        //REMOVE OLD LOGIN AND ADD NEW ONE
        if (edited) {
            workspace.loginsTableView.getSelectionModel().getSelectedItem().setName(loginNew.getName());
            workspace.loginsTableView.getSelectionModel().getSelectedItem().setUserID(loginNew.getUserID());
            workspace.loginsTableView.getSelectionModel().getSelectedItem().setPassword(loginNew.getPassword());
            workspace.loginsTableView.getSelectionModel().getSelectedItem().setWebsite(loginNew.getWebsite());
            workspace.loginsTableView.getSelectionModel().getSelectedItem().setNote(loginNew.getNote());
            
            //REMOVE THE DUPLICATE LOGIN AND INFORM USER
            boolean duplicate = app.getData().removeDublicate();
            if(duplicate){
                app.getMessageDialog().show("Login Exist", "Similar Login already Exist. \nClick on Name to sort by Name.");
            }
        }

        //REFRESH THE TABLE
        workspace.loginsTableView.refresh();
    }

    /**
     * CONTROLLER TO HANDLE VIEW BUTTON DISPLAY THE LOGIN IN NEW WINDOW
     */
    public void handleViewButton() {
        //GET THE WORKSPACE
        PasswordManagerGUI workspace = app.getGui();

        //GET SELECTED LOGIN
        Login login = workspace.loginsTableView.getSelectionModel().getSelectedItem();

        //VIEW THE LOGIN
        app.getViewDialog().display(login);
    }

    /**
     * HCONTROLLER TO HANDLE WEBSITE BUTTON OPENS THE LOGIN WEBSITE IN DEFAUL
     * BROWSER
     */
    public void handleWebsiteButton() {
        //GET THE WORKSPACE
        PasswordManagerGUI workspace = app.getGui();

        //GET SELECTED LOGIN
        Login login = workspace.loginsTableView.getSelectionModel().getSelectedItem();

        //OPEN THE LINK
        if (login.getWebsite().length() > 0) {
            try {
                URI uri = new URI(login.getWebsite());
                Desktop.getDesktop().browse(uri);
            } catch (Exception ex) {
                app.getMessageDialog().show("Error", "Make sure you have entered the correct website");
            }
        } else {
            app.getMessageDialog().show("No Website", "Please enter the website to open in future");
        }
    }

    /**
     * CONTROLLER TO HANDLE SELECTION IN TABLE VIEW OF PASSWORD PANE
     */
    public void handleSlideSelected() {
        //GET THE WORKSPACE
        PasswordManagerGUI workspace = app.getGui();

        //ENABLE THE SINGLE SLIDE SELECTION OPTION IF SLIDE IS SELECTED
        int selectedIndex = workspace.loginsTableView.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            workspace.singleLoginSelected(true);
        } else {
            workspace.singleLoginSelected(false);
        }
    }

    /**
     * This method will import data from csv file. User will prompt to open csv
     * file.
     */
    public void handleImportRequest() {
        //PROMPT USER TO SELECT FILE
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(path.getUserHomePath()));
        fc.setTitle("Select File");
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("csv", "*.csv"));
        File selectedFile = fc.showOpenDialog(app.getStage());
        if (selectedFile != null) {
            try {
                app.getDataManager().importData(app.getData(), selectedFile);
                
                //REMOVE THE DUPLICATE
                app.getData().removeDublicate();
                
                //INFORM USER THAT IMPORT IS FINISHED
                app.getMessageDialog().show("Conformation", "Import finished successfully.");
            } catch (Exception ex) {
                //REMOVE THE DUPLICATE
                app.getData().removeDublicate();
                
                //SHOW MESSAGE
                app.getMessageDialog().show("Error", "Error in importing data");
            }
        }
        //REFRESH THE TABLE
        app.getGui().loginsTableView.refresh();
    }

    /**
     * This method will export all the login detail to csv file. User will
     * prompt to save the file location.
     */
    public void handleExportRequest() {
        // PROMPT THE USER FOR A FILE NAME
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(path.getUserHomePath()));
        fc.setTitle("Select Location");
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("csv", "*.csv"));
        File selectedFile = fc.showSaveDialog(app.getStage());
        if (selectedFile != null) {
            try {
                app.getDataManager().exportData(app.getData(), selectedFile);
                //INFORM USER THAT IMPORT IS FINISHED
                app.getMessageDialog().show("Conformation", "Export finished successfully.");
            } catch (IOException ex) {
                app.getMessageDialog().show("Error", "Error in exporting data");
            }
        }

    }

    /**
     * This method will backup the program data. User will asked for the
     * location.
     */
    public void handleBackupRequest() {
        // PROMPT THE USER FOR A FILE NAME
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(path.getUserHomePath()));
        fc.setTitle("Select Location");
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Backup File", "*.pmbkp"));
        File selectedFile = fc.showSaveDialog(app.getStage());
        if (selectedFile != null) {
            try {
                app.getDataManager().backupData(app.getData(), selectedFile);
                //INFORM USER THAT IMPORT IS FINISHED
                app.getMessageDialog().show("Conformation", "Backup finished successfully.");
            } catch (IOException ex) {
                app.getMessageDialog().show("Error", "Error while backing up data");
            }
        }

    }

    /**
     * This method will restore the program data form previously created backup.
     * User will asked for the location.
     */
    public void handleRestoreRequest() {
        //PROMPT USER TO SELECT FILE
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(path.getUserHomePath()));
        fc.setTitle("Select File");
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Backup File", "*.pmbkp"));
        File selectedFile = fc.showOpenDialog(app.getStage());
        if (selectedFile != null) {
            try {
                app.getDataManager().restoreData(app.getData(), selectedFile);
                
                //REMOVE THE DUPLICATE
                app.getData().removeDublicate();
                
                //UPDATE WORKSPACE
                app.getGui().reloadUserOptionGUI();
                app.getGui().reloadBackupGUI();
                app.getGui().loginsTableView.refresh();
                app.getGui().pane.setCenter(app.getGui().backupPane);
                
                //INFORM USER THAT IMPORT IS FINISHED
                app.getMessageDialog().show("Conformation", "Restore finished successfully.");
            } catch (Exception ex) {
                app.getMessageDialog().show("Error", "Error while restoring data");
            }
        }
    }
    
    /**
     * Handler to disable the scheduled backup. 
     */
    public void handleDiableBackupSchedule() {
        //DISABLE THE SCHEDUEL
        app.getData().getBackupDetail().resetBackup();
    }
    
    /**
     * Handler to setup new backup schedule. 
     * @param location = backup location.
     * @param days = how many days after backup should run.
     */
    public void enableBackupSchedule(String location, int days) {
        app.getData().getBackupDetail().setSchedule(true);
        app.getData().getBackupDetail().setLocation(location);
        app.getData().getBackupDetail().setDays(days);
        app.getMessageDialog().show("Conformation", "Backup Scheduled successfully.");
    }
}
