/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.workspace;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import passwordmanager.PasswordManager;
import passwordmanager.dataclasses.HelpText;
import static passwordmanager.style.PasswordManagerStyle.CLASS_STEP_HEADER;
import static passwordmanager.style.PasswordManagerStyle.CLASS_TEXT;

/**
 *
 * @author tirth
 */
public class PMHelpGUI {

    PasswordManager app;
    PasswordManagerController controller;

    //COMPONENT
    TabPane tabPane;

    Tab passwordTab;
    Tab userTab;
    Tab impoExpoTab;
    Tab backupTab;
    Tab licenseTab;

    // SP means ScrollPane
    ScrollPane passwordSP;
    ScrollPane userSP;
    ScrollPane impoExpoSP;
    ScrollPane backupSP;
    ScrollPane licenseSP;

    Text passwordText;
    Text userText;
    Text impoExpoText;
    Text backupText;
    Text licenseText;

    Label topLabel;

    //FIANL PANE
    BorderPane finalPane;

    public PMHelpGUI(PasswordManager app) {
        this.app = app;

        controller = app.getController();

        //INITIATE THE COMPONENT
        initVariables();

        //RENDER GUI 
        renderGUI();

        //INITIATE CONTROLLERS
        initTexts();

        //INITIATE STYLE
        initStyle();
    }

    /**
     * This method will provide the pane needed for help pane.
     *
     * @return
     */
    public BorderPane getHelpGUI() {
        return finalPane;
    }

    private void initVariables() {
        tabPane = new TabPane();

        passwordTab = new Tab();
        userTab = new Tab();
        impoExpoTab = new Tab();
        backupTab = new Tab();
        licenseTab = new Tab();

        passwordSP = new ScrollPane();
        userSP = new ScrollPane();
        impoExpoSP = new ScrollPane();
        backupSP = new ScrollPane();
        licenseSP = new ScrollPane();

        passwordText = new Text();
        userText = new Text();
        impoExpoText = new Text();
        backupText = new Text();
        licenseText = new Text();

        topLabel = new Label("Help Wizard");

        //FIANL PANE
        finalPane = new BorderPane();
    }

    private void renderGUI() {
        //ADD TABS TO THE TABPANE
        tabPane.getTabs().add(passwordTab);
        tabPane.getTabs().add(userTab);
        tabPane.getTabs().add(impoExpoTab);
        tabPane.getTabs().add(backupTab);
        //tabPane.getTabs().add(licenseTab);

        //PUT SCROLLPANE INTO EACH TAB
        passwordTab.setContent(passwordSP);
        userTab.setContent(userSP);
        impoExpoTab.setContent(impoExpoSP);
        backupTab.setContent(backupSP);
        licenseTab.setContent(licenseSP);

        //SET TAB TITLE
        passwordTab.setText("Passwords");
        userTab.setText("User Option");
        impoExpoTab.setText("Import & Export");
        backupTab.setText("Backup & Restore");
        licenseTab.setText("License");

        //MAKE TAB NOT CLOSABLE
        passwordTab.setClosable(false);
        userTab.setClosable(false);
        impoExpoTab.setClosable(false);
        backupTab.setClosable(false);
        licenseTab.setClosable(false);

        //PUT TEXT INTO THE SCROLLPANE
        passwordSP.setContent(passwordText);
        userSP.setContent(userText);
        impoExpoSP.setContent(impoExpoText);
        backupSP.setContent(backupText);
        licenseSP.setContent(licenseText);

        //FINAL PANE SETUP
        finalPane.setTop(topLabel);
        finalPane.setCenter(tabPane);

        //WIDTH AND HEIGH RENDERING
        topLabel.prefWidthProperty().bind(finalPane.widthProperty());
        topLabel.setAlignment(Pos.CENTER);
        tabPane.prefWidthProperty().bind(finalPane.widthProperty());
        passwordSP.prefWidthProperty().bind(finalPane.widthProperty());
        userSP.prefWidthProperty().bind(finalPane.widthProperty());
        impoExpoSP.prefWidthProperty().bind(finalPane.widthProperty());
        backupSP.prefWidthProperty().bind(finalPane.widthProperty());
        licenseSP.prefWidthProperty().bind(finalPane.widthProperty());
    }

    private void initTexts() {
        HelpText texts = new HelpText();
        passwordText.setText(texts.getPasswordText());
        userText.setText(texts.getUserText());
        impoExpoText.setText(texts.getImpoExpoText());
        backupText.setText(texts.getBackupText());
        licenseText.setText(texts.getLicenseText());
    }

    private void initStyle() {
        //HEADER
        topLabel.getStyleClass().add(CLASS_STEP_HEADER);

        //ADD THE TEXT STYLE
        tabPane.getStyleClass().add(CLASS_TEXT);
    }
}
