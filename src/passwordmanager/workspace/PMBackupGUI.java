/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.workspace;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import passwordmanager.PasswordManager;
import static passwordmanager.style.PasswordManagerStyle.CLASS_BUTTON;
import static passwordmanager.style.PasswordManagerStyle.CLASS_STEP_HEADER;
import static passwordmanager.style.PasswordManagerStyle.CLASS_TEXT;
import static passwordmanager.style.PasswordManagerStyle.CLASS_USER_LABEL;

/**
 *
 * @author tirth
 */
public class PMBackupGUI {

    //SAVE APP
    PasswordManager app;
    PasswordManagerController controller;

    //COMPONENT FOR USER PANE
    Label topLabel;
    Label infoLabel1;
    Label infoLabel2;
    Label infoLabel3;
    Label backupLabel;
    Label restoreLabel;
    Button backupButton;
    Button restoreButton;

    //SCHEDULE COMPONENT
    CheckBox scheduleCB;
    Label scheduleLabel;
    Label daysLabel;
    Label locationLabel;
    Label backupStatusLabel;
    Label statusLabel;
    TextField locationText;
    Button scheduleButton;
    Spinner<Integer> spinner;

    //PANES
    HBox scheduleDayPane;
    HBox locationPane;
    HBox statusPane;
    VBox pane;
    GridPane form;

    //FIANL PANE
    BorderPane finalPane;

    public PMBackupGUI(PasswordManager app) {
        this.app = app;

        controller = app.getController();

        //INITIATE THE COMPONENT
        initVariables();

        //RENDER GUI 
        renderGUI();

        //INITIATE CONTROLLERS
        initControls();

        //INITIATE STYLE
        initStyle();

        //SCHEDULE BACKUP UI
        initSchedule();
    }

    /**
     * This method will provide the pane needed for backup pane.
     *
     * @return
     */
    public BorderPane getBackupGUI() {
        return finalPane;
    }

    private void initVariables() {
        //INIT LABELS
        topLabel = new Label("Backup & Restore Wizard");
        infoLabel1 = new Label("Welcome to Backup & Restore Wizard.");
        infoLabel2 = new Label("Backup will help you to create a backup of your program data.");
        infoLabel3 = new Label("Restore will help you to restore data from previously created backup.");
        backupLabel = new Label("Start Backup");
        restoreLabel = new Label("Start Import");
        backupButton = new Button("Backup");
        restoreButton = new Button("Restore");

        //SCHEDULE COMPONENT
        scheduleCB = new CheckBox("Schedule Backup");
        scheduleLabel = new Label("Schedule Backup every ");
        daysLabel = new Label(" Days.");
        locationLabel = new Label("Backup Location: ");
        backupStatusLabel = new Label("Backup Status: ");
        statusLabel = new Label();
        locationText = new TextField();
        locationText.setPromptText("Backup Location:");
        scheduleButton = new Button("Schedule");
        scheduleButton.setDisable(true);
        spinner = new Spinner<Integer>();
        SpinnerValueFactory<Integer> valueFactory
                = //
                new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 30, 1);
        spinner.setValueFactory(valueFactory);

        //INIT PANES
        scheduleDayPane = new HBox(10);
        locationPane = new HBox(15);
        statusPane = new HBox(15);
        pane = new VBox(25);
        form = new GridPane();
        finalPane = new BorderPane();
    }

    private void renderGUI() {
        //MAKING FORM
        form.add(backupLabel, 0, 0);
        form.add(backupButton, 1, 0);
        form.add(restoreLabel, 0, 1);
        form.add(restoreButton, 1, 1);

        //SCHEDULE PANE
        scheduleDayPane.getChildren().add(scheduleLabel);
        scheduleDayPane.getChildren().add(spinner);
        scheduleDayPane.getChildren().add(daysLabel);
        locationPane.getChildren().add(locationLabel);
        locationPane.getChildren().add(locationText);
        statusPane.getChildren().add(backupStatusLabel);
        statusPane.getChildren().add(statusLabel);

        //MAKING PANE
        pane.getChildren().add(infoLabel1);
        pane.getChildren().add(infoLabel2);
        pane.getChildren().add(infoLabel3);
        pane.getChildren().add(form);
        pane.getChildren().add(scheduleCB);
        pane.getChildren().add(scheduleDayPane);
        pane.getChildren().add(locationPane);
        pane.getChildren().add(scheduleButton);
        pane.getChildren().add(statusPane);

        //FINAL PANE
        finalPane.setTop(topLabel);
        finalPane.setCenter(pane);

        //SIZE THE GUI
        pane.prefWidthProperty().bind(finalPane.widthProperty());
        pane.prefHeightProperty().bind(finalPane.heightProperty());
        pane.setFillWidth(true);
        pane.setPadding(new Insets(10, 10, 10, 10));
        form.setHgap(50);
        form.setVgap(25);
        locationPane.prefWidthProperty().bind(pane.widthProperty());
        HBox.setHgrow(locationText, Priority.ALWAYS);
        topLabel.prefWidthProperty().bind(finalPane.widthProperty());
        topLabel.setAlignment(Pos.CENTER);
    }

    private void initControls() {
        backupButton.setOnAction(e -> {
            controller.handleBackupRequest();
        });
        restoreButton.setOnAction(e -> {
            controller.handleRestoreRequest();
        });
        scheduleCB.selectedProperty().addListener(e -> {
            if (scheduleCB.isSelected()) {
                disabelStatusControls(false);                
                if (!locationText.getText().isEmpty()) {
                    controller.enableBackupSchedule(locationText.getText(), spinner.getValue());
                    statusLabel.setText("Backup is schedule");
                }
            } else {
                disabelStatusControls(true);
                statusLabel.setText("Backup is not schedule");
                controller.handleDiableBackupSchedule();
            }
        });
        locationText.textProperty().addListener((observable, oldValue, newValue) -> {
            enableSubmitButton();
        });
        spinner.valueProperty().addListener((observable, oldValue, newValue) ->{
            enableSubmitButton();
        });
        scheduleButton.setOnAction(e -> {
            controller.enableBackupSchedule(locationText.getText(), spinner.getValue());
            statusLabel.setText("Backup is scheduled");
        });
    }

    private void initStyle() {
        //ADD THE TEXT STYLE
        pane.getStyleClass().add(CLASS_TEXT);

        //LABEL
        infoLabel1.getStyleClass().add(CLASS_USER_LABEL);
        infoLabel2.getStyleClass().add(CLASS_USER_LABEL);
        infoLabel3.getStyleClass().add(CLASS_USER_LABEL);
        backupLabel.getStyleClass().add(CLASS_USER_LABEL);
        restoreLabel.getStyleClass().add(CLASS_USER_LABEL);

        //BUTTON 
        backupButton.getStyleClass().add(CLASS_BUTTON);
        restoreButton.getStyleClass().add(CLASS_BUTTON);

        //HEADER
        topLabel.getStyleClass().add(CLASS_STEP_HEADER);

        //SCHEDULE LABEL 
        scheduleCB.getStyleClass().add(CLASS_USER_LABEL);
        //scheduleLabel.getStyleClass().add(CLASS_USER_LABEL);
        //daysLabel.getStyleClass().add(CLASS_USER_LABEL);
        //locationLabel.getStyleClass().add(CLASS_USER_LABEL);
        //backupStatusLabel.getStyleClass().add(CLASS_USER_LABEL);
        //statusLabel.getStyleClass().add(CLASS_USER_LABEL);
        //scheduleButton.getStyleClass().add(CLASS_USER_LABEL);
    }

    private void initSchedule() {
        //disable the control
        disabelStatusControls(true);

        if (app.getData().getBackupDetail().isSchedule()) {
            scheduleCB.selectedProperty().setValue(true);
            spinner.getValueFactory().setValue(app.getData().getBackupDetail().getDays());
            locationText.setText(app.getData().getBackupDetail().getLocation());
            statusLabel.setText("Last Backup created on " + app.getData().getBackupDetail().getBackupDate().toString());
            scheduleButton.setDisable(true);
        } else {
            statusLabel.setText("Backup is not schedule");
        }
    }

    /**
     * This method disable the control of the status component.
     *
     * @param value
     */
    private void disabelStatusControls(boolean value) {
        locationText.setDisable(value);
        spinner.setDisable(value);
    }

    private void enableSubmitButton() {
        if (locationText.getText().isEmpty()) {
            scheduleButton.setDisable(true);
        } else {
            scheduleButton.setDisable(false);
        }
    }

}
