/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.dataclasses;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author tirth
 */
public class BackupObject {
    String location;
    int days;
    Date backupDate;
    boolean schedule;
    
    /**
     * Date need to be between 1 to 30.
     * @param location
     * @param day 
     */
    public BackupObject(String location, int day){
        this.location = location;
        if(day<=0){this.days = 1;}
        if(day>30){this.days = 30;}
        this.days = day;
        this.backupDate = Calendar.getInstance().getTime();
        this.schedule = false;
    }
    
    public BackupObject(String location, int date, Date backupDate){
        this.location = location;
        if(date<=0){this.days = 1;}
        if(date>30){this.days = 30;}
        this.days = date;
        this.backupDate = backupDate;
        this.schedule = false;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int date) {
        this.days = date;
    }

    public Date getBackupDate() {
        return backupDate;
    }

    public void setBackupDate(Date backupDate) {
        this.backupDate = backupDate;
    }

    public boolean isSchedule() {
        return schedule;
    }

    public void setSchedule(boolean schedule) {
        this.schedule = schedule;
    }
    
    public void resetBackup(){
        this.location = "";
        this.days = 1;
        this.backupDate = Calendar.getInstance().getTime();
        this.schedule = false;
    }
    
    public boolean isBackupDue(Date date){
        int time = (int) (date.getTime() - backupDate.getTime())/(1000*60*60*24);
        if(time > this.days){return true;}
        else{return false;}
    }
    
    public static void main(String [] args){
        BackupObject b = new BackupObject("new",1);
        Date now = Calendar.getInstance().getTime();
        Date pre = Calendar.getInstance().getTime();
        pre.setDate(27);
        b.setBackupDate(pre);
        System.out.println("Date " + b.getBackupDate());
        System.out.println("due" +  b.isBackupDue(now) + " date" + now);
    }
    
}
