/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.dataclasses;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author tirth
 */
public class Login implements Comparable{
    private StringProperty name;
    private StringProperty userID;
    private StringProperty password;
    private StringProperty website;
    private String note;
    
    public Login(String name, String userID, String password, String website){
        this.name = new SimpleStringProperty(name);
        this.userID = new SimpleStringProperty(userID);
        this.password = new SimpleStringProperty(password);
        if(website.isEmpty()){website = "";}
        this.website = new SimpleStringProperty(website);
        this.note = "";
    }
    
     public Login(String name, String userID, String password, String website, String note){
        this.name = new SimpleStringProperty(name);
        this.userID = new SimpleStringProperty(userID);
        this.password = new SimpleStringProperty(password);
        if(website.isEmpty()){website = "";}
        if(note.isEmpty()){note ="";}
        this.website = new SimpleStringProperty(website);
        this.note = note;
    }

    public StringProperty getNameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.setValue(name);
    }

    public StringProperty getUserIDProperty() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID.setValue(userID);
    }

    public StringProperty getPasswordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.setValue(password);
    }

    public StringProperty getWebsiteProperty() {
        return website;
    }

    public void setWebsite(String website) {
        this.website.setValue(website);
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getName() {
        return name.getValue();
    }

    public String getUserID() {
        return userID.getValue();
    }

    public String getPassword() {
        return password.getValue();
    }

    public String getWebsite() {
        return website.getValue();
    }
    
    @Override
    public boolean equals(Object o){
        if(this == o){return true;}
        if(! (o instanceof Login)){return false;}
        Login that = (Login) o;
        return this.getName().equalsIgnoreCase(that.getName()) && this.getUserID().equalsIgnoreCase(that.getUserID());
    }
    
    @Override
    public String toString(){
        String s = "";
        s = s + "Name:     " + this.getName() + "\n";
        s = s + "UserID:   " + this.getUserID() + "\n";
        s = s + "Password: " + this.getPassword() + "\n";
        s = s + "Website:  " + this.getWebsite() + "\n";
        s = s + "Note:     " + this.getNote() + "\n";
        return s;
    }

    @Override
    public int compareTo(Object o) {
        Login that = (Login) o;
        return this.getName().compareTo(that.getName());
    }
}
