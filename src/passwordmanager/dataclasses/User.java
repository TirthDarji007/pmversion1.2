/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.dataclasses;

/**
 *
 * @author tirth
 * @version 1.0
 */
public class User {
    String name = "";
    String userID; 
    String password;
    String email = ""; 
    String recovery  = "";
    
    public User(String id, String pass, String recovery){
        this.userID = id;
        this.password = pass;
        this.recovery = recovery;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRecovery() {
        return recovery;
    }
   
}
