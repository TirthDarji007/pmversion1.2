/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.dataclasses;

import java.util.Iterator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import passwordmanager.PasswordManager;

/**
 *
 * @author tirth
 */
public class PasswordManagerData {
    
    PasswordManager app;
    ObservableList<Login> logins;
    User user = null;
    BackupObject backupDetail = new BackupObject("",1);
    
    public PasswordManagerData (PasswordManager app){
        this.app = app;
        logins = FXCollections.observableArrayList();
    }
    
    /**
     * This method adds the login to data. 
     * @param add 
     */
    public void addLogin(String name, String userID, String password, String website){
        Login add = new Login(name,userID,password,website);
        logins.add(add);
    }
    
    /**
     * This method adds the login to data. 
     * @param add 
     */
    public void addLogin(String name, String userID, String password, String website, String note){
        Login add = new Login(name,userID,password,website,note);
        logins.add(add);
    }
    
    /**
     * This method adds the non duplicate login to data. 
     * @param add 
     */
    public void addNonDuplicateLogin(String name, String userID, String password, String website){
        Login add = new Login(name,userID,password,website);
        if (!hasLogin(add)){
            logins.add(add);
        }        
    }
    
    /**
     * This method adds the non duplicate login to data. 
     * @param add 
     */    
    public void addNonDuplicateLogin(String name, String userID, String password, String website, String note){
        Login add = new Login(name,userID,password,website,note);
        if (!hasLogin(add)){
            logins.add(add);
        }        
    }
    
    /**
     * This method adds the non duplicate login to data.
     * It returns true if the login is added. 
     * @param add 
     */
    public boolean addLogin(Login add){
        boolean ans = false;
        if (!hasLogin(add)){
            logins.add(add);
            ans = true;
        }
        return ans;
    }
    
    /**
     * This method returns the logins in data
     * @return 
     */
    public ObservableList<Login> getLogins() {
        return logins;
    }
    
    /**
     * This method removes the login from data
     * @param login 
     */
    public void removeLogin(Login login){
        for(int i=0; i < logins.size();i++){
            Login l = logins.get(i);
            if(l.equals(login)){
                logins.remove(i);
            }
        }
    }
    
    /**
     * This method clears the data. 
     */
    public void resetData(){
        logins.clear();
    }
    
    /**
     * This method returns true if login contains in data
     * @param input
     * @return 
     */
    public boolean hasLogin(Login input){
        for(Login l: logins){
            if (l.equals(input)){
                return true;
            }
        }
        return false;
    }
    
    /**
     * This method goes through the data and removes all the duplicate logins.
     * It return true if duplicate is found and deleted.
     */
    public boolean removeDublicate(){
        Login current;
        boolean ans = false;
        for(int i =0; i < logins.size(); i++){
            current = logins.get(i);
            for(int j = (i+1); j < logins.size(); j++){
                if(current.equals(logins.get(j))){
                    logins.remove(j);
                    ans = true;
                }
            }
        }
        return ans;
    }
    
    /**
     * This method swap the login in given position
     * @param a
     * @param b 
     */
    public void swap (int a, int b){
        Login temp = logins.get(a); 
        logins.set(a, logins.get(b));
        logins.set(b, temp);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BackupObject getBackupDetail() {
        return backupDetail;
    }

    public void setBackupDetail(BackupObject backupDetail) {
        this.backupDetail = backupDetail;
    }
    
}
