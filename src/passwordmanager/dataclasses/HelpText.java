/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.dataclasses;

/**
 *
 * @author tirth
 */
public class HelpText {
    
    String passwordText = ""
            + "Name:\n"
            + "This field is use to identify the login. \n"
            + "It is a required field and must be entered.\n"
            + "\n"
            + "UserID:\n"
            + "This is field where you can enter the userID for your login.\n"
            + "It is a required field and must be entered.\n"
            + "\n"
            + "Password:\n"
            + "This is field where you can enter the password for your login.\n"
            + "It is a required field and must be entered.\n"
            + "\n"
            + "Website:\n"
            + "This is field where you can enter the website address.\n"
            + "It is recommend to copy and past the URL from web browser.\n"
            + "\n"
            + "Note:\n"
            + "This is field where you can enter the note about that login.\n"
            + "If the login contains security question and answers then enter it in Note.\n"
            + "\n"
            + "Add:\n"
            + "Click on this button to add new login information.\n"
            + "\n"
            + "Remove:\n"
            + "Click on this buttton to remove selected login.\n"
            + "This action cannot be undone.\n"
            + "Once deleted, it cannot come back unless if you have kept backup. \n"
            + "\n"
            + "Edit:\n"
            + "Click this button to edit or update the login detail.\n"
            + "You can also double click on login to open edit dialog.\n"
            + "\n"
            + "View:\n"
            + "Click on this button to view the login information.\n"
            + "You cannot edit detail from this dialog.\n"
            + "\n"
            + "Go to Website:\n"
            + "If you have saved the website then clicking on this link will open the saved website.\n"
            + "\n"
            + "Same Login:\n"
            + "Two logins will be consider same if two of them has the same name and same userID.\n"
            + "\n";
    
    String UserText = ""
            + "Name:\n"
            + "User's name to identify the individual.\n"
            + "It is recommanded to enter the name.\n"
            + "\n"
            + "UserID:\n"
            + "This is field where you can enter the userID which will be use to login into the application.\n"
            + "It is a required field and must be entered.\n"
            + "\n"
            + "Password:\n"
            + "This is field where you can enter the password which will be use to login into the application.\n"
            + "It is a required field and must be entered.\n"
            + "\n"
            + "Email:\n"
            + "Enter ypu email address to keep your profile up to date.\n"
            + "We will not send any promotion to provided email.\n"
            + "It is recommend to keep this information.\n"
            + "\n"
            + "Recovery Code:\n"
            + "You can enter the recovery code of your own choice.\n"
            + "Recovery code will be use to log into the application as a replacement of userID and password.\n"
            + "Do not give recovery code to anyone else. Keep code, which you will always remember.\n"
            + "If you have forgotten the userID, password and recovery code then there is no way to recover old data.\n"
            + "It is a required field and must be entered.\n"
            + "\n"
            + "Submit:\n"
            + "If you have made any changes to user login settings the click submit button to save those changes.\n"
            + "If you do not click submit button after changed the settings, then settings will not be saved.\n"
            + "\n";
    
    String impoExpoText = ""
            + "Import:\n"
            + "Click this button to import login detail from the csv file.\n"
            + "Import will ask you to choose the desire file you want to import.\n"
            + "Import assumes that selected csv file has correct csv format.\n"
            + "While importing data, if error happens then user will be notified and importing will stop.\n"
            + "Import will not enter the duplicate login. Go to Password tab for same login information\n"
            + "See below for accepted csv format.\n"
            + "\n"
            + "Export:\n"
            + "Click this button to export all login detail to csv file.\n"
            + "Export will ask you to select the directory where you want save the file and the name of the file.\n"
            + "While exporting data, if error happens then user will be notified and importing will stop.\n"
            + "See below for csv file format.\n"
            + "\n"
            + "CSV Format:\n"
            + "Import & Export will use the same format as shown below.\n"
            + "This is how the csv file should be: \n"
            + "for each login: name,userID,password,website,note \n"
            + "Not following this format will result in error.\n"
            + "When importing, it is recmmanded to have all values of login.\n"
            + "\n";
    
    String backupText = ""
            + "Backup:\n"
            + "Backup will backup all the data files to given location.\n"
            + "Backup will ask you to select the directory and file name for the backup.\n"
            + "It is recommanded to keep backup.\n"
            + "Keep the backup file on secret place.\n"
            + "Unchecking this will remove the automatic shedule backup.\n"
            + "\n"
            + "Restore:\n"
            + "Restore will resote the data from previously created backup file.\n"
            + "Restore will ask user to choose backup file, from which data will be restored.\n"
            + "Resoter will also restore the previous user login settings: name, userID, Password, Email and Recovery Code.\n"
            + "Restore will alse the restore scheduled data from selected file.\n"
            + "Use restore when you have completely lost your data and need to recover everything from file.\n"
            + "\n"
            + "Schedule Backup:\n"
            + "Check this box if you want to schedule automatic backup.\n"
            + "After checking this box, user need to enter the fixed backup location and click schedule button.\n"
            + "Checking this box will not enable the automatic schedule if the backup location is not given.\n"
            + "User will be notified when automatic backup is schedule or changes has been saved for automatic bakcup.\n"
            + "\n"
            + "Backup Location:\n"
            + "Enter the defaul directory location for backup.\n"
            + "Automatic bakcup will not run without the backup location.\n"
            + "Backup Lcocation example for windows user: \"C:\\Users\\tirth\\Password Manager\\backup\"\n"
            + "\n"
            + "Schedule:\n"
            + "Click on this button to save the changes of automatic backup.\n"
            + "Backup information will not be saved if the schedule button is not clicked.\n"
            + "User will be notified when changes has been saved.\n"
            + "\n";
    
    String licenseText = "";

    public String getPasswordText() {
        return passwordText;
    }

    public String getUserText() {
        return UserText;
    }

    public String getImpoExpoText() {
        return impoExpoText;
    }

    public String getBackupText() {
        return backupText;
    }

    public String getLicenseText() {
        return licenseText;
    }
    
}
