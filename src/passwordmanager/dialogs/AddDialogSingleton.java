/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.dialogs;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import passwordmanager.dataclasses.Login;
import static passwordmanager.dialogs.AppMessageDialogSingleton.singleton;

/**
 *
 * @author tirth
 */
public class AddDialogSingleton extends Stage {

    // HERE'S THE SINGLETON OBJECT
    static AddDialogSingleton singleton = null;

    //HERE IS THE ADDED LOGIN FROM USER
    Login response = null;
    boolean isedited = false;

    //Start the dialod 
    Label nameLabel;
    Label userIDLabel;
    Label passwordLabel;
    Label websiteLabel;
    Label noteLabel;
    Label infoLabel;
    TextField nameText;
    TextField userIDText;
    TextField passwordText;
    TextField websiteText;
    TextArea noteText;
    Button enterButton;
    GridPane form;
    VBox pane;

    Scene scene;

    private AddDialogSingleton() {
    }

    public static AddDialogSingleton getSingleton() {
        if (singleton == null) {
            singleton = new AddDialogSingleton();
        }
        return singleton;
    }

    /**
     * IT WILL RETURN THE RESPONSE FROM USER
     *
     * @return
     */
    public Login getResponse() {
        return response;
    }

    /**
     * Returns true if the enter button is clicked and login created
     *
     * @return
     */
    public boolean isIsedited() {
        return isedited;
    }

    /**
     * CALL THIS METHOD TO INITIATE THE DIALOG SINGLETON. WITHOUT CALLING THIS
     * METHOD, DISPALY WILL DISPLAY NOTHING. THIS METHOD CREATE THE DIALOG.
     *
     * @param owner
     */
    public void init(Stage owner) {
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);

        //INTIATE THE LABEL AND TEXT FIELD
        nameLabel = new Label("Name:");
        userIDLabel = new Label("UserID:");
        passwordLabel = new Label("Password:");
        websiteLabel = new Label("Website:");
        noteLabel = new Label("Note:");
        infoLabel = new Label("Name, UserID and Password must be entered");
        nameText = new TextField();
        userIDText = new TextField();
        passwordText = new TextField();
        websiteText = new TextField();
        noteText = new TextArea();

        //INITIATE BUTTON AND PANES
        enterButton = new Button("Enter");
        enterButton.setDisable(true);
        enterButton.setAlignment(Pos.CENTER);
        form = new GridPane();
        pane = new VBox();

        //RENDER GUI
        form.add(infoLabel, 1, 0);
        form.add(nameLabel, 0, 1);
        form.add(userIDLabel, 0, 2);
        form.add(passwordLabel, 0, 3);
        form.add(websiteLabel, 0, 4);
        form.add(noteLabel, 0, 5);
        form.add(nameText, 1, 1);
        form.add(userIDText, 1, 2);
        form.add(passwordText, 1, 3);
        form.add(websiteText, 1, 4);
        form.add(noteText, 1, 5);
        form.setHgap(10);
        form.setVgap(10);

        pane.getChildren().add(form);
        pane.getChildren().add(enterButton);
        pane.setSpacing(10);
        pane.setFillWidth(true);
        pane.setPadding(new Insets(10, 10, 10, 10));
        form.setStyle("-fx-background-color: #e39971;-fx-font-size:14pt");
        pane.setStyle("-fx-background-color: #e39971");
        enterButton.setStyle("-fx-font-size:14pt");

        //SET SCENE
        scene = new Scene(pane);
        this.setScene(scene);

        //INITIATE THE CONTROLLERS
        enterButton.setOnAction(e -> {
            handleEnterRequest();
        });
        nameText.textProperty().addListener((observable, oldValue, newValue) -> {
            eableEnterButton();
        });
        userIDText.textProperty().addListener((observable, oldValue, newValue) -> {
            eableEnterButton();
        });
        passwordText.textProperty().addListener((observable, oldValue, newValue) -> {
            eableEnterButton();
        });
        websiteText.textProperty().addListener((observable, oldValue, newValue) -> {
            eableEnterButton();
        });
        noteText.textProperty().addListener((observable, oldValue, newValue) -> {
            eableEnterButton();
        });
    }

    /**
     * Display the add dialog. Make sure to call init method before display
     * method. TO ADD NEW LOGIN.
     */
    public void display() {
        //RESET THE RESPONSE EACH TIME DIALOG IS OPENED
        response = null;
        isedited = false;

        //RESET THE TEXT BEFORE OPENING DIALOG
        resetText();

        //SET TITAL
        setTitle("Add Login Information");

        //CHANGE BUTTON NAME
        enterButton.setText("Add");

        //DIABLE THE ENTER BUTTON
        enterButton.setDisable(true);

        //SHOW THE DIALOG AND WAIT FOR RESPONSE
        showAndWait();
    }

    /**
     * TO EDIT THE LOGIN.
     *
     * @param login
     */
    public void display(Login login) {
        //RESET THE RESPONSE EACH TIME DIALOG IS OPENED
        response = null;
        isedited = false;

        //SET THE TEXT BEFORE OPENING DIALOG
        setText(login);

        //SET TITAL
        setTitle("Edit Login Information");

        //CHANGE BUTTON NAME
        enterButton.setText("Save");

        //DIABLE THE ENTER BUTTON
        enterButton.setDisable(true);

        //SHOW THE DIALOG AND WAIT FOR RESPONSE
        showAndWait();
    }

    /**
     * THIS METHOD CLEARS THE TEXT FROM FORM.
     */
    private void resetText() {
        nameText.clear();
        userIDText.clear();
        passwordText.clear();
        websiteText.clear();
        noteText.clear();
    }

    /**
     * THIS METHOD WILL ENABLE THE ENTER BUTTON IF NAME, USERID AND PASSWORD IS
     * ENTERED.
     */
    private void eableEnterButton() {
        if ((!nameText.getText().isEmpty())
                && (!userIDText.getText().isEmpty())
                && (!passwordText.getText().isEmpty())) {

            enterButton.setDisable(false);

        } else {
            enterButton.setDisable(true);
        }
    }

    private void setText(Login login) {
        nameText.setText(login.getName());
        userIDText.setText(login.getUserID());
        passwordText.setText(login.getPassword());
        websiteText.setText(login.getWebsite());
        noteText.setText(login.getNote());
    }

    private void handleEnterRequest() {
        Login login = new Login(nameText.getText(), userIDText.getText(),
                passwordText.getText(), websiteText.getText(),
                noteText.getText());

        response = login;
        isedited = true;
        AddDialogSingleton.this.close();
    }
}
