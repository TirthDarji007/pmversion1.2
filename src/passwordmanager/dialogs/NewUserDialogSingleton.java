/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.dialogs;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import passwordmanager.dataclasses.Login;
import passwordmanager.dataclasses.User;
import static passwordmanager.dialogs.AddDialogSingleton.singleton;

/**
 *
 * @author tirth
 */
public class NewUserDialogSingleton extends Stage{
    
    // HERE'S THE SINGLETON OBJECT
    static NewUserDialogSingleton singleton = null;
    
    //USER OBJECT TO RETURN
    User response = null;
    
    //BUILD DIALOG BOX
    Label infoLabel;
    Label nameLabel; 
    Label userIDLabel;
    Label passwordLabel; 
    Label emailLabel;
    Label recoveryLabel;
    Label recoveryInfo;
    TextField nameText;
    TextField userIDText;
    TextField passwordText;
    TextField emailText;
    TextField recoveryText;
    Button enterButton;
    GridPane form;
    VBox pane;
    Scene scene;
    
    private  NewUserDialogSingleton() {}
    
    public static NewUserDialogSingleton getSingleton() {
	if (singleton == null)
	    singleton = new NewUserDialogSingleton();
	return singleton;
    }

    public User getResponse() {
        return response;
    }
  
    /**
     * CALL THIS METHOD TO INITIATE THE DIALOG SINGLETON. 
     * WITHOUT CALLING THIS METHOD, DISPALY WILL DISPLAY NOTHING.
     * THIS METHOD CREATE THE DIALOG.
     * @param owner 
     */
    public void init(Stage owner){
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        //INTIATE THE LABEL AND TEXT FIELD
        nameLabel = new Label("Name:");
        userIDLabel = new Label("UserID:");
        passwordLabel = new Label("Password:");
        emailLabel = new Label("Email:");
        recoveryLabel = new Label("Recovery Code:");
        infoLabel = new Label("UserID, Password and Recovery Code must be entered.");
        recoveryInfo = new Label("Recovery Code will be used if you forgot the password.");
        nameText = new TextField();
        userIDText = new TextField();
        passwordText = new TextField();
        emailText = new TextField();
        recoveryText = new TextField();
        enterButton = new Button("Create User");
        form = new GridPane();
        pane = new VBox();
        
        form.add(nameLabel, 0, 0);
        form.add(nameText, 1, 0);
        form.add(userIDLabel, 0, 1);
        form.add(userIDText, 1, 1);
        form.add(passwordLabel, 0, 2);
        form.add(passwordText, 1, 2);
        form.add(emailLabel, 0, 3);
        form.add(emailText, 1, 3);
        form.add(recoveryLabel, 0, 4);
        form.add(recoveryText, 1, 4);
        form.setHgap(10);
        form.setVgap(10);
        
        pane.getChildren().add(infoLabel);
        pane.getChildren().add(recoveryInfo);
        pane.getChildren().add(form);
        pane.getChildren().add(enterButton);
        pane.setSpacing(10);
        pane.setFillWidth(true);
        pane.setPadding(new Insets(10,10,10,10));
        form.setStyle("-fx-background-color: #e39971;-fx-font-size:14pt");
        pane.setStyle("-fx-background-color: #e39971;-fx-font-size:14pt");
        enterButton.setStyle("-fx-font-size:14pt");
        
        //SET SCENE
        scene = new Scene(pane);
        this.setScene(scene);
        
        //INITIATE THE CONTROLLERS
        enterButton.setOnAction(e->{
            handleEnterButton();
        });
        scene.setOnKeyReleased(e->{
            if(e.getCode() == KeyCode.ENTER && (!enterButton.isDisabled())){
                handleEnterButton();
            }
        });
        nameText.textProperty().addListener((observable, oldValue, newValue)->{
            eableEnterButton();
        });
        userIDText.textProperty().addListener((observable, oldValue, newValue)->{
            eableEnterButton();
        });
        passwordText.textProperty().addListener((observable, oldValue, newValue)->{
            eableEnterButton();
        });
        emailText.textProperty().addListener((observable, oldValue, newValue)->{
            eableEnterButton();
        });
        recoveryText.textProperty().addListener((observable, oldValue, newValue)->{
            eableEnterButton();
        });
    }
    
    /**
     * Display the add dialog. Make sure to call init method before display method.
     * TO ADD NEW LOGIN.
     */
    public void display() {
        //RESET THE RESPONSE EACH TIME DIALOG IS OPENED
        response = null;
        
        //RESET THE TEXT BEFORE OPENING DIALOG
        resetText();
        
        //SET TITAL
        setTitle("Create New User");
        
        //DIABLE THE ENTER BUTTON
        enterButton.setDisable(true);
        
        //SHOW THE DIALOG AND WAIT FOR RESPONSE
        showAndWait();
    }
    
    /**
     * THIS METHOD CLEARS THE TEXT FROM FORM. 
     */
    private void resetText() {
        nameText.clear();
        userIDText.clear();
        passwordText.clear();
        emailText.clear();
        recoveryText.clear();
    }
    
    /**
     * THIS METHOD WILL ENABLE THE ENTER BUTTON IF NAME, USERID AND PASSWORD IS ENTERED. 
     */
    private void eableEnterButton(){
        if ( (!userIDText.getText().isEmpty()) && 
                (!passwordText.getText().isEmpty()) && 
                    (!recoveryText.getText().isEmpty()) ){
            
            
            enterButton.setDisable(false);
            
        }
        else{
            enterButton.setDisable(true);
        }
    }
    
    private void handleEnterButton(){
        User user = new User(userIDText.getText(),passwordText.getText(),recoveryText.getText());
            user.setName(nameText.getText());
            user.setEmail(emailText.getText());
            response = user;
            NewUserDialogSingleton.this.close();
    }
}
