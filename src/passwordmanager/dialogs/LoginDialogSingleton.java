/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.dialogs;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import passwordmanager.dataclasses.User;
import static passwordmanager.dialogs.NewUserDialogSingleton.singleton;

/**
 *
 * @author tirth
 */
public class LoginDialogSingleton extends Stage {

    // HERE'S THE SINGLETON OBJECT
    static LoginDialogSingleton singleton = null;
    User user;
    boolean response = false;

    //BUILD DIALOG BOX
    Label userIDLabel;
    Label passwordLabel;
    Label infoLabel;
    Hyperlink recover;
    TextField userIDText;
    TextField passwordText;
    Button loginButton;
    GridPane form;
    VBox pane;
    Scene scene;

    //NEW WINDOW FOR RECOVER 
    VBox recoverPane;
    Label recoverPaneLabe;
    TextField recoverPaneText;
    Button enterButton;

    private LoginDialogSingleton() {
    }

    public static LoginDialogSingleton getSingleton() {
        if (singleton == null) {
            singleton = new LoginDialogSingleton();
        }
        return singleton;
    }

    public boolean getResponse() {
        return response;
    }

    /**
     * CALL THIS METHOD TO INITIATE THE DIALOG SINGLETON. WITHOUT CALLING THIS
     * METHOD, DISPALY WILL DISPLAY NOTHING. THIS METHOD CREATE THE DIALOG.
     *
     * @param owner
     */
    public void init(Stage owner) {
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);

        //INTIATE THE LABEL AND TEXT FIELD
        userIDLabel = new Label("UserID:");
        passwordLabel = new Label("Password:");
        recover = new Hyperlink("Forgot the userID and password?");
        infoLabel = new Label("Please enter the UserId and Password.");
        userIDText = new TextField();
        passwordText = new TextField();
        loginButton = new Button("Login");
        form = new GridPane();
        pane = new VBox();

        form.add(userIDLabel, 0, 0);
        form.add(userIDText, 1, 0);
        form.add(passwordLabel, 0, 1);
        form.add(passwordText, 1, 1);
        form.setHgap(10);
        form.setVgap(10);

        pane.getChildren().add(infoLabel);
        pane.getChildren().add(form);
        pane.getChildren().add(loginButton);
        pane.getChildren().add(recover);
        pane.setSpacing(10);
        pane.setFillWidth(true);
        pane.setPadding(new Insets(10, 10, 10, 10));
        form.setStyle("-fx-background-color: #e39971;-fx-font-size:14pt");
        pane.setStyle("-fx-background-color: #e39971;-fx-font-size:14pt");
        loginButton.setStyle("-fx-font-size:14pt");

        //RECOVER PANE
        recoverPane = new VBox();
        recoverPaneLabe = new Label("Enter the Recovery Code");
        recoverPaneText = new TextField();
        enterButton = new Button("Enter");
        recoverPane.getChildren().add(recoverPaneLabe);
        recoverPane.getChildren().add(recoverPaneText);
        recoverPane.getChildren().add(enterButton);
        recoverPane.setSpacing(10);
        recoverPane.setFillWidth(true);
        recoverPane.setPadding(new Insets(10, 10, 10, 10));
        recoverPane.setStyle("-fx-background-color: #e39971;-fx-font-size:14pt");
        enterButton.setStyle("-fx-font-size:14pt");

        //SET SCENE
        scene = new Scene(pane);
        this.setScene(scene);

        //CONTROLLERS 
        loginButton.setOnAction(e -> {
            if ((userIDText.getText().equalsIgnoreCase(user.getUserID()))
                    && (passwordText.getText().equals(user.getPassword()))) {

                response = true;
                LoginDialogSingleton.this.close();
            } else {
                //PASSWORD DID NOT MATCH INFORM AND ASK TO REENTER
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Login Error");
                alert.setHeaderText(null);
                alert.setContentText("Password did not Match.");
                alert.showAndWait();
            }

        });
        recover.setOnAction(e -> {
            this.setScene(new Scene(recoverPane));
        });
        recoverPane.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                handleEnterButton();
            }
        });
        enterButton.setOnAction(e -> {
            handleEnterButton();
        });
        userIDText.textProperty().addListener((observable, oldValue, newValue) -> {
            eableEnterButton();
        });
        passwordText.textProperty().addListener((observable, oldValue, newValue) -> {
            eableEnterButton();
        });
        scene.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                if (!loginButton.isDisabled()) {
                    if ((userIDText.getText().equalsIgnoreCase(user.getUserID()))
                            && (passwordText.getText().equals(user.getPassword()))) {

                        response = true;
                        LoginDialogSingleton.this.close();
                    } else {
                        //PASSWORD DID NOT MATCH INFORM AND ASK TO REENTER
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle("Login Error");
                        alert.setHeaderText(null);
                        alert.setContentText("Password did not Match. Click OK to Retry");
                        alert.showAndWait();
                    }
                }
            }
        });

    }

    public void display(User user) {
        //RESET USER AND RESPONSE
        this.user = user;
        response = false;

        //RESET THE TEXT BEFORE OPENING DIALOG
        resetText();

        //SET TITAL
        setTitle("Login");

        //DIABLE THE ENTER BUTTON
        loginButton.setDisable(true);

        //SHOW THE DIALOG AND WAIT FOR RESPONSE
        showAndWait();
    }

    /**
     * THIS METHOD CLEARS THE TEXT FROM FORM.
     */
    private void resetText() {
        userIDText.clear();
        passwordText.clear();
        recoverPaneText.clear();
    }

    /**
     * THIS METHOD WILL ENABLE THE ENTER BUTTON IF NAME, USERID AND PASSWORD IS
     * ENTERED.
     */
    private void eableEnterButton() {
        if ((!userIDText.getText().isEmpty())
                && (!passwordText.getText().isEmpty())) {

            loginButton.setDisable(false);

        } else {
            loginButton.setDisable(true);
        }
    }

    /**
     * This is the controller to handle recover pane enter button.
     */
    private void handleEnterButton() {
        if (recoverPaneText.getText().equals(user.getRecovery())) {
            response = true;
            LoginDialogSingleton.this.close();
        } else {
            //RECOVER CODE DID NOT MATCH INFORM AND ASK TO REENTER
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Recover Code Error");
            alert.setHeaderText(null);
            alert.setContentText("Recover code did not Match.");
            alert.showAndWait();
        }
    }
}
