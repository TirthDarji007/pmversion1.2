/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.dialogs;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * This is the conformation dialog. Code Check application use to conform
 * delete.
 *
 * @author tirth
 */
public class YesNoLabelSingleton extends Stage {

    // HERE'S THE SINGLETON OBJECT
    static YesNoLabelSingleton singleton = null;

    // HERE ARE THE DIALOG COMPONENTS
    VBox messagePane;
    Scene messageScene;
    Label messageLabel;
    Button yesButton;
    Button noButton;
    HBox buttons;

    //RESPONSE OF THE USER 
    Boolean response = false;

    private YesNoLabelSingleton() {
    }

    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static YesNoLabelSingleton getSingleton() {
        if (singleton == null) {
            singleton = new YesNoLabelSingleton();
        }
        return singleton;
    }

    public Boolean getResponse() {
        return response;
    }

    public void init(Stage owner) {
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);

        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        messageLabel = new Label();
        buttons = new HBox(20);

        //BUTTON
        yesButton = new Button("Yes");
        noButton = new Button("No");
        noButton.setOnAction(e -> {
            YesNoLabelSingleton.this.close();
        });
        yesButton.setOnAction(e -> {
            response = true;
            YesNoLabelSingleton.this.close();
        });

        // WE'LL PUT EVERYTHING HERE
        buttons.getChildren().add(yesButton);
        buttons.getChildren().add(noButton);
        buttons.setAlignment(Pos.CENTER);

        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(buttons);
        messagePane.setStyle("-fx-font-size:14pt");

        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(80, 60, 80, 60));
        messagePane.setSpacing(20);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }

    public void show(String title, String message) {

        //RESET THE REPONSE
        response = false;

        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);

        // SET THE MESSAGE TO DISPLAY TO THE USER
        messageLabel.setText(message);

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }
}
