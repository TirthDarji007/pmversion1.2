/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.dialogs;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import passwordmanager.dataclasses.Login;
import passwordmanager.dataclasses.User;

/**
 *
 * @author tirth
 */
public class DialogTester extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        LoginDialogSingleton dialog = LoginDialogSingleton.getSingleton();
        dialog.init(primaryStage);
        dialog.display(new User("tirth","darji","me"));
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
