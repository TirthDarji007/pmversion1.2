/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passwordmanager.dialogs;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import passwordmanager.dataclasses.Login;

/**
 *
 * @author tirth
 */
public class ViewDialogSingleton extends Stage{
    // HERE'S THE SINGLETON OBJECT
    static ViewDialogSingleton singleton = null;
    
    //Start the dialod 
    Label nameLabel;
    Label userIDLabel;
    Label passwordLabel;
    Label websiteLabel;
    Label noteLabel;
    Label infoLabel;
    Label nameText;
    Label userIDText;
    Label passwordText;
    Label websiteText;
    Text noteText;
    ScrollPane notePane;
    GridPane form;
    VBox pane; 
    
    Scene scene;
    
    private  ViewDialogSingleton() {}
    
    public static ViewDialogSingleton getSingleton() {
	if (singleton == null)
	    singleton = new ViewDialogSingleton();
	return singleton;
    }
    
    
    /**
     * CALL THIS METHOD TO INITIATE THE DIALOG SINGLETON. 
     * WITHOUT CALLING THIS METHOD, DISPALY WILL DISPLAY NOTHING.
     * THIS METHOD CREATE THE DIALOG.
     * @param owner 
     */
    public void init(Stage owner) {
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        //INTIATE THE LABEL AND TEXT FIELD
        nameLabel = new Label("Name:");
        userIDLabel = new Label("UserID:");
        passwordLabel = new Label("Password:");
        websiteLabel = new Label("Website:");
        noteLabel = new Label("Note:");
        infoLabel = new Label("Name, UserID and Password must be entered");
        nameText = new Label();
        userIDText = new Label();
        passwordText = new Label();
        websiteText = new Label();
        noteText = new Text();
        notePane = new ScrollPane();
        notePane.setContent(noteText);
        notePane.setFitToWidth(true);
        notePane.setFitToHeight(true);
        
        //INITIATE BUTTON AND PANES
        form = new GridPane();
        pane = new VBox();
        
        //RENDER GUI
        form.add(nameLabel, 0, 1);
        form.add(userIDLabel, 0, 2);
        form.add(passwordLabel, 0, 3);
        form.add(websiteLabel, 0, 4);
        form.add(noteLabel, 0, 5);
        form.add(nameText, 1, 1);
        form.add(userIDText, 1, 2);
        form.add(passwordText, 1, 3);
        form.add(websiteText, 1, 4);
        form.add(notePane, 1, 5);
        form.setHgap(10);
        form.setVgap(10);
        nameLabel.prefWidthProperty().bind(form.widthProperty().divide(3));
        nameText.prefWidthProperty().bind(form.widthProperty());
        GridPane.setFillWidth(notePane, true);
        //GridPane.setFillHeight(form, true);
        form.prefWidthProperty().bind(pane.widthProperty());
        
        pane.getChildren().add(form);
        pane.setSpacing(10);
        pane.setFillWidth(true);
        pane.setPadding(new Insets(10,10,10,10));
        form.setStyle("-fx-background-color: #e39971;-fx-font-size:14pt");
        pane.setStyle("-fx-background-color: #e39971");
        notePane.setStyle("-fx-background: #e39971");
        
        //SET SCENE
        setTitle("View Login Information");
        scene = new Scene(pane,700,500);
        this.setScene(scene);
    }
    
    /**
     * Display the add dialog. Make sure to call init method before display method.
     */
    public void display(Login login) {
        
        //SET THE TEXT BEFORE OPENING DIALOG
        setText(login);
        
        //SHOW THE DIALOG AND WAIT FOR RESPONSE
        showAndWait();
    }
    
    /**
     * THIS METHOD CLEARS THE TEXT FROM FORM. 
     */
    private void setText(Login login) {
        nameText.setText(login.getName());
        userIDText.setText(login.getUserID());
        passwordText.setText(login.getPassword());
        websiteText.setText(login.getWebsite());
        noteText.setText(login.getNote());
    }
    

}
